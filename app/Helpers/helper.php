<?php
function debug($array=[],$exit=0){
    echo '<pre>';
    print_r($array);
    echo '</pre>';
    if($exit){
        exit();
    }
}

function calculate_time_span($date){

    $hour1 = 0; $hour2 = 0;
    $date1 = $date;
    $date2 = date('Y-m-d');
    $datetimeObj1 = new DateTime($date1);
    $datetimeObj2 = new DateTime($date2);
    $interval = $datetimeObj1->diff($datetimeObj2);

    if($interval->format('%a') > 0){
        $hour1 = $interval->format('%a')*24;
    }
    if($interval->format('%h') > 0){
        $hour2 = $interval->format('%h');
    }
    return ($hour1 + $hour2);
}

function getIP() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
function array_to_csv_download($array, $filename = "export.csv", $delimiter=",") {
    // open raw memory as file so no temp files needed, you might run out of memory though
    $f = fopen('php://memory', 'w');
    // loop over the input array
    foreach ($array as $line) {
        // generate csv lines from the inner arrays
        fputcsv($f, $line, $delimiter);
    }
    // reset the file pointer to the start of the file
    fseek($f, 0);
    // tell the browser it's going to be a csv file
    header('Content-Type: application/csv');
    // tell the browser we want to save it instead of displaying it
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    // make php send the generated csv lines to the browser
    fpassthru($f);
}
function csv_to_array($filename='', $delimiter=',')
{
    if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
        {
            if(!$header)
                $header = $row;
            else
                //$data[] = array_combine($header, $row);
                $data[] = $row;
        }
        fclose($handle);
    }
    return $data;
}
function hasAccess(){
    if(Auth::check()){
        if(Auth::user()->role == 'admin'){
            return true;
        }
    }
    return false;
}
function isAdmin(){
    if(Auth::check()){
        if(Auth::user()->role == 'admin'){
            return true;
        }
    }
    return false;
}
function getSubUsersId($parent_id){
    $data = [];
    $data[] = $parent_id;
    $users = \DB::table('users')->select('id')->where('parent_id','=',$parent_id)->get();
    if(sizeof($users) >0){
        foreach($users as $user){
            $data[] = $user->id;
        }
    }
    return $data;
}
function _mail($data){
    //$dataEmail = ['to'=>'','subject'=>'',$cc=>'','body'=>'','files'=>[],'sender'=>email_sender,'replyTo'=>email_reply_to];
    try{
        \Mail::send([], [], function ($message) use ($data){
            $message->to($data['to'])
                ->subject($data['subject'])
                ->replyTo('noreply@remindme.me')
                ->from ('noreply@remindme.me','remindme.me')
                ->sender('noreply@remindme.me');
            if(isset($data['cc']) && trim(!empty($data['cc']))){
                $message->cc($cc);
            }
            if(isset($data['files']) && !empty($data['files']) && sizeof($data['files']) > 0){
                for( $i=0; $i<count($data['files']); $i++ ){
                    $message->attach($data['files'][$i]); // change i to $i
                }
            }
            $message = $message->setBody($data['body'],'text/html');
        });
    }catch(Exception $e){
        echo $e->getMessage();
        exit;
        return false;
    }
    return ['Operation'=>'True','Message'=>'Email Sent Successfully.'];
}
function drawErrors(){
    $returnStr = '';

    if (\Session::has('errors')){
        $errorsStr = '';
        $errors = \Session::get('errors')->toArray();
        foreach($errors as $error){
            $errorsStr .= $error[0].'<br>';
        }
        $returnStr = '<div class="alert alert-danger errordivMain"><span  onclick="hideErrorDiv()" class="pull-right"  style="color:#933432; font-size: 20px;line-height: 15px; cursor: pointer;" >×</span>'.$errorsStr.'</div>';
    }
    if(\Session::has('success') && !empty(\Session::get('success'))){
        $returnStr = '  <div class="alert alert-success errordivMain"><span  onclick="hideErrorDiv()" class="pull-right" style="color:#2b542c; font-size: 20px;line-height: 15px;cursor: pointer;" >×</span>'.\Session::get('success').'</div>';
    }
    return $returnStr;
}
function _encode($str){
    $str = base64_encode($str);
    $str = str_replace('=','gcc_ab',$str);
    $str = str_replace('Z','jazznju',$str);
    $str = gzcompress($str, 9);
    $str = strtr(base64_encode($str), '+/=', '._-');
    return urlencode($str);
}
function _decode($str){
    $str = base64_decode(strtr(urldecode($str), '._-', '+/='));
    $str = gzuncompress($str);
    $str = str_replace('gcc_ab','=',$str);
    $str = str_replace('jazznju','Z',$str);

    $str = base64_decode($str);

    return $str;
}
/*function calculate_time_span($date){

    $hour1 = 0; $hour2 = 0;
    $date1 = $date;
    $date2 = date('Y-m-d H:i:s');
    $datetimeObj1 = new DateTime($date1);
    $datetimeObj2 = new DateTime($date2);
    $interval = $datetimeObj1->diff($datetimeObj2);

    if($interval->format('%a') > 0){
        $hour1 = $interval->format('%a')*24;
    }
    if($interval->format('%h') > 0){
        $hour2 = $interval->format('%h');
    }
    return ($hour1 + $hour2);
}*/
function sendSMS($number, $text){
    $url = "https://platform.clickatell.com/messages/http/send?apiKey=id654VaWSfWQfiv4LiXPaw==&to=".$number."&content=".urlencode($text);
    $sendSmsRes = file_get_contents($url);
    return $sendSmsRes;
}
function checkIsSubscribe(/*$request, $next*/)
{
    //debug(\Auth::User(),1);

        if (Auth::check() && \Route::currentRouteName() != 'Payment') {
            if (empty(Auth::User()->stripe_customer_id) || empty(Auth::User()->subscription_id) || empty(Auth::User()->subscription_date))
            {
                $userRegisterDate = Auth::user()->created_at;
                $hoursSinceCreated = calculate_time_span($userRegisterDate);
                $daysCreated = abs(round($hoursSinceCreated / 24));
                $remainingDays = 7 - $daysCreated;
                $remainingDays = $remainingDays < 1 ? 0 : $remainingDays;
                if ($hoursSinceCreated > '168' && \Route::currentRouteName() != 'Payment' && $remainingDays == 0) {
                    return Redirect::to(route('Payment'))->withErrors(['Sorry Your Free Account has been expire. Please subscribe for continue access your account. Thanks'])->send();
                    exit;

                }
            }else {
                // print_r($getBillings->billing_expire_date);exit();
                $lasBillingDate = Auth::User()->exp_date;
                $date_now = date("Y-m-d"); // this format is string comparable


                $date1 = date_create(date('Y-m-d'));
                $date2 = date_create(date('Y-m-d', strtotime($lasBillingDate)));

                //difference between two dates
                $diff = date_diff($date1, $date2);
                $remainingDaysOfBilling = $diff->format("%a");

                if ($remainingDaysOfBilling < 0) {
                    $remainingDaysOfBilling = '0';
                }
                if ($date_now > $lasBillingDate && \Route::currentRouteName() != 'Payment') {

                    return Redirect::to(route('Payment'))->withErrors(['Sorry Your Previous billing has been expire. Please subscribe again for continue access your account. Thanks'])->send();
                }
            }


        }
           // return $next($request);



}