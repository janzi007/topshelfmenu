<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class state extends Model
{

    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    protected $table = 'state';
}
