<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use form;
use state;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use File;
use lists;
class AccountController extends Controller
{

    public function updateProfile(Request $request)
    {
        if(Auth::id()!=''){

            $user_id =  Auth::User()->id;
            $username=($request->input('username')!='')?$request->input('username'):'';
            $email=($request->input('email')!='')?$request->input('email'):'';
            $company = ($request->input('company_name')!='')?$request->input('company_name'):'';
            $address = ($request->input('address')!='')?$request->input('address'):'';
            $address2 = ($request->input('address2')!='')?$request->input('address2'):'';
            $website = ($request->input('website')!='')?$request->input('website'):'';
            $state = ($request->input('state')!='')?$request->input('state'):'';
            $city = $request->input('city');
            $zip_code = $request->input('zip_code');


            /*
              $user_data3 = DB::table('users')->where('email', $email)->where('id', '!=' , $user_id)->get();
              if(count($user_data3)==0){*/
            $last_id=DB::table('users')->where('id', $user_id)
                ->update(
                    ['username'=>$username,'email'=>$email,'website'=>$website,'company_name'=>$company,'address2'=>$address2,'address'=>$address,'city'=>$city,'state'=>$state,'zip_code'=>$zip_code]
                );

            if(Input::hasFile('image'))
            {

                $file=Input::file('image');
                $random_name=time();
                $destinationPath='profile/';
                $extension=$file->getClientOriginalExtension();
                $filename=$random_name.'.'.$extension;
                $byte=File::size($file); //get size of file
                $uploadSuccess=Input::file('image')->move($destinationPath,$filename);


                $user_data = DB::table('users')->where('id', $user_id)->first();
                if($user_data->image!=''){
                    File::delete(public_path().'/profile/'.$user_data->image); // Delete old
                }
                /* $file = Input::file('image');
                $name = time() . '-' . $file->getClientOriginalName();
                $file = $file->move(public_path() . '/profile/', $name);*/
                $last_id=DB::table('users')->where('id', $user_id)
                    ->update(['image' => $filename]);
                Session::put('member_logo', $filename);
            }

            Session::flash('success', 'Your account information has been updated.');
            return  redirect()->to('/bussiness');
            /*}else{
            Session::flash('error', 'Email already exists.');
            return  redirect()->to('dashboard/account/account');

            }*/

        }else{
            return  redirect()->to('/login');
        }
    }
            public function bussiness()
            {
                $state =DB::table('state')->get();

                return view('settings.bussiness',['state'=>$state]);
            }

            public function password()
            {
                return view('settings.password');
            }

    public function upadtePassword(Request $request)
    {
        if(Auth::id()!=''){
            $user_id = Auth::User()->id;
            $old_password = $request->input('old_password');
            $new_password = $request->input('new_password');
            $user_data3 = DB::table('users')->where('password', md5($old_password))->where('id', '=' , $user_id)->get();
            if(count($user_data3)>0){
                $last_id=DB::table('users')->where('id', $user_id)
                    ->update(
                        ['password' => md5($new_password)]
                    );
                Session::flash('pass_success', 'Your password information has been updated.');
                return  redirect()->to('/password');
            }else{
                Session::flash('pass_error', 'Current password does not match.');
                return  redirect()->to('/password');
            }
        }else{
            return  redirect()->to('/login');
        }
    }
        public function payment()
            {
                return view('settings.payment');
            }
        public function paymentPost(Request $request)
        {
            if(Auth::id()!='') {

                $user_id = Auth::User()->id;
                $customerId =$request->input('stripe_customer_id');
                $csv = $request->input('subscription_id');
               /*debug($user_id);
               debug($customerId);
               debug($csv);
               exit();*/
                /*$expDate = ($request->input('exp_date') != '') ? $request->input('exp_date') : '';*/
                DB::table('users')->where('id', $user_id)
                    ->update(
                        ['stripe_customer_id'=>$customerId,'subscription_id'=>$csv/*,'exp_date'=>$expDate*/]
                    );
                Session::flash('success', 'Your account information has been updated.');
                return  redirect()->to('/payment');

            }
            else{
                return  redirect()->to('/login');
            }
            }
}
