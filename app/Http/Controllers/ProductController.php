<?php

namespace App\Http\Controllers;

use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
class ProductController extends Controller
{
   /* public function select_product(Request $request)
    {

        if(Auth::Id()!=''){
            $user_id=Auth::User()->id;
            $pre_sel = DB::table('users')->where('id', $user_id)->first();
            $product_type=DB::table('admin_type')->get();
            $product_brand=DB::table('admin_brand')->orderBy('name', 'asc')->get();
            $product_data='';
            $query='';

            $query = DB::table('admin_products')
                ->join('admin_brand', 'admin_products.brand', '=', 'admin_brand.brand_id');
            if(!empty($request->pro_type)){
                $query->where('admin_products.product_type','=', $request->pro_type);
            }
            if(!empty($request->brand_type)){
                $query->where('admin_products.brand','=', $request->brand_type);
            }
            if(!empty($request->search_key)){
                $query->Where('admin_products.pro_title', 'Like', "%$request->search_key%");
                $query->orWhere('admin_products.short_description', 'Like', "%$request->search_key%");
                $query->orWhere('admin_products.long_description', 'Like', "%$request->search_key%");
            }
            //Remove pre selected product
            if($pre_sel->selected_product!=''){
                $proId=$pre_sel->selected_product;
                $myArray = explode(',', $proId);
                for($t=0;$t<count($myArray);$t++){
                    $query->where('admin_products.id','!=', $myArray[$t]);
                }
            }
            $product_data=$query->get();


            return view('manage_menu/select-product',compact('product_data','product_type','product_brand'));

        }else{
            return  redirect()->to('auth/login');
        }
    }*/

    public function loadMore(Request $request){
        checkIsSubscribe();
        $user_id=Auth::user()->id;
        $pre_sel = DB::table('users')->where('id', $user_id)->first();

        $product_type = DB::table('admin_type')->where('parentid', 'NULL')->get();
        $product_brand=DB::table('admin_brand')->orderBy('name', 'asc')->get();
        $product_data='';
        $query='';
        $sel_class='';
        $checked='';
        $query = DB::table('admin_products')
            ->join('admin_brand', 'admin_products.brand', '=', 'admin_brand.brand_id');


        if(!empty($request->pro_type)){
            $query->where('admin_products.product_type','=', $request->pro_type);
            $query->orWhere('admin_products.sub_cat','=', $request->pro_type);
        }
        if(!empty($request->brand_type)){
            $query->where('admin_products.brand','=', $request->brand_type);
        }
        if(!empty($request->search_key)){
            $query->Where('admin_products.pro_title', 'Like', "%$request->search_key%");
            $query->orWhere('admin_products.short_description', 'Like', "%$request->search_key%");
            $query->orWhere('admin_products.long_description', 'Like', "%$request->search_key%");
        }
        //Remove pre selected product
        /*if($pre_sel->selected_product!=''){
            $proId=$pre_sel->selected_product;
            $myArray = explode(',', $proId);
            for($t=0;$t<count($myArray);$t++){
                $query->where('admin_products.id','!=', $myArray[$t]);
            }
        }*/

        $product_data=$query->paginate(12);
        $url = route("home");
        $html='';
        $k=1;
        foreach ($product_data as $value) {
            $img='';
            if($value->image==''){
                $img=$url.'/images/cannabis.png';
            }
            else{
                $img=$url.'/admin_product_image/'.$value->image;
            }
            if(!empty($request->selPro)){
                $myArray = explode(',', $request->selPro);
                if (in_array($value->id, $myArray)){
                    $sel_class='select_product_active';
                    $checked='checked';
                }else{
                    $sel_class='';
                    $checked='';
                }
            }
            $html.=' <li>
            <div class="item-box">

            <div class="shop-item '.$sel_class.'" id="shop_item_'.$value->id.'">
            <div class="item-img">
            <div class="border__btm"><img src="'.$img.'" style="height:100%;" /></div>
            <div class="shop-item-info">
            <div class="shop-item-name"><a href="#">'.$value->pro_title.' </a></div>
            <div class="shop-item-cate"><span>'.$value->name.'</span></div>
            </div>

            </div>
            <div class="item-mask">
            <div class="item-mask-detail">
            <div class="item-mask-detail-ele">
            <a class="select____btn">  <div class="checkbox m-0">
            <label class="p-0">
            <input type="checkbox" class="checkboxes" name="sel_pro[]" '.$checked.' id="sel_id_'.$value->id.'" value="'.$value->id.'" onclick="selectSingle('.$value->id.');" >
            <span class="cr"><i class="cr-icon fa fa-check"></i></span>Select</label>
            </div></a>

            </div>
            </div>
            </div>
            </div>

            </div>
            </li>  ';
            $k++;
        }
        if ($request->ajax()) {
            return $html;
        }
        return view('manage_menu/select-product',compact('product_data','product_type','product_brand'));
    }

    public function publish_product(Request $request)
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $user_id=Auth::User()->id;
            if($request->action=='add') {
                //debug($request->all(),1);
                $save_status ='0';
                $pro_title=($request->input('pro_title')!='')?$request->input('pro_title'):'';
                $short_desc=($request->input('short_desc')!='')?$request->input('short_desc'):'';
                $pro_desc=($request->input('pro_desc')!='')?$request->input('pro_desc'):'';
                $pro_price='0';
                $pro_type=($request->input('pro_type')!='')?$request->input('pro_type'):'';
                $sub_cat=($request->input('sub_cat')!='')?$request->input('sub_cat'):'';
                $brand=($request->input('brand')!='')?$request->input('brand'):'';
                $add_date=date("Y-m-d");
                //brand check  exits
                $brand_check = DB::table('admin_brand')->where('user_id', '1')->where('name', $brand)->get();
                if(count($brand_check)==0){
                    $last_brand=DB::table('admin_brand')->insertGetId(['user_id'=>'1','name'=>$brand] );
                }else{
                    $brand_get = DB::table('admin_brand')->where('user_id', '1')->where('name', $brand)->first();
                    $last_brand=$brand_get->brand_id;
                }

                //category check  exits
                if($pro_type!=''){
                    $cat_check = DB::table('admin_type')->where('type_name', $pro_type)->get();
                    if(count($cat_check)==0){
                        $last_cat=DB::table('admin_type')->insertGetId(['user_id'=>$user_id,'type_name'=>$pro_type] );
                    }else{
                        $cat_get = DB::table('admin_type')->where('user_id', '1')->where('type_name', $pro_type)->first();
                        $last_cat=$cat_get->type_id;
                    }
                }else{
                    $last_cat='';
                }
                $cub_category='';
                //subcategory check  exits
                if($sub_cat!=''){
                    $cat_check2 = DB::table('admin_type')->where('type_name', $sub_cat)->get();
                    if(count($cat_check2)==0){
                        $last_cat=DB::table('admin_type')->insertGetId(['user_id'=>'1','type_name'=>$sub_cat] );
                    }else{
                        $cat_get = DB::table('admin_type')->where('type_name', $sub_cat)->first();
                        $cub_category=$cat_get->type_id;
                    }
                }else{
                    $cub_category='';
                }

                $last_id=DB::table('products')->insertGetId(
                    ['user_id' => $user_id,'save_status'=>$save_status,'pro_title'=>addslashes($pro_title),'pro_type'=>addslashes($last_cat),'sub_cat'=>$cub_category,'brand'=>addslashes($last_brand),'short_desc' => addslashes($short_desc),'pro_desc' => addslashes($pro_desc),'pro_price' => $pro_price,
                        'status'=>'1','add_date'=>$add_date,'pro_image'=>'','prod_view'=>'0']  );

                //Add QTY and Price
                if($last_id!=''){
                    $min_price='500000';
                    for($i=1;$i<=5;$i++){
                        $question = ($request->input('question_'.$i)!='')?$request->input('question_'.$i):'';
                        $ans = ($request->input('ans_'.$i)!='')?$request->input('ans_'.$i):'';
                        if($question!=''){
                            $insert=DB::table('products_qty')->insertGetId(
                                ['pro_id' => $last_id,'qty' => addslashes($question),'price' =>addslashes($ans)]);

                            if($ans<$min_price){
                                $min_price=$ans;
                            }
                        }
                    }

                    $last_id2=DB::table('products')->where('prod_id', $last_id)->update(['pro_price' => $min_price]);
                    $proId='';
                    //Add selected product
                    $pre_sel = DB::table('users')->where('id', $user_id)->first();


                    if($pre_sel->selected_product==''){
                        $proId=$request->input('pro_id');

                    }else{
                        /*$proId=$pre_sel->selected_product.','.$request->input('pro_id');*/
                        $proId=$pre_sel->selected_product;
                    }
                    //debug(explode(',',$proId),1);
                    $last_id4=DB::table('users')->where('id', $user_id)->update(['selected_product' => $proId]);

                    //Effects
                    for($k=1;$k<=5;$k++){
                        $name = ($request->input('effects_title_'.$k)!='')?$request->input('effects_title_'.$k):'';
                        $price = ($request->input('effects_per_'.$k)!='')?$request->input('effects_per_'.$k):'';
                        $insert=DB::table('strain_attributes')->insertGetId(['prod_id' => $last_id,'type'=>'1','name' => addslashes($name),'percentage' =>addslashes($price)] );
                    }
                    //Image upload
                    if(!empty($request->file('image_path')) && count($request->file('image_path')) > 0){

                        if(!is_dir('product_images/'.$last_id.'/')){
                            mkdir('product_images/'.$last_id.'/', 0777, true);

                        }

                        $destinationPath='product_images/'.$last_id.'/';
                        //
                        foreach($request->file('image_path') as $key=>$image)
                        {

                            $random_name = $key.rand().$image->getClientOriginalName();
                            //$newPathWithName = $destinationPath.$random_name;
                            $image->move($destinationPath, $random_name);
                            if($key == 0){
                                DB::table('products')->where('prod_id', $last_id)->update(array('pro_image' => $random_name));
                            }
                            DB::table('products_image')->insertGetId(['pro_id' => $last_id,'image' => $random_name]);

                        }
                    }

                }


                $input = $request->input('pro_id');
                $list = $request->input('selected_id');
                $array1 = Array($input);
                $array2 = explode(',', $list);
                $array3 = array_diff($array2, $array1);

                $output = implode(',', $array3);
                if($output!=''){
                    $again_loadId = explode(',', $output);
                    //Reload again
                    $pro_id='';
                    $sel_pro=$again_loadId;
                    $sel_product='';
                    $query = DB::table('admin_products')
                        ->join('admin_brand', 'admin_products.brand', '=', 'admin_brand.brand_id')
                        ->join('admin_type', 'admin_products.product_type', '=', 'admin_type.type_id');
                    for($k=0;$k<count($sel_pro);$k++){
                        $pro_id.=$sel_pro[$k].',';
                        $query->orWhere('admin_products.id','=', $sel_pro[$k]);
                    }
                    $sel_product=$query->get();
                    $pro_id=rtrim($pro_id,',');

                }else{
                    $sel_product='';
                    $pro_id='';
                }
                Session::flash('success', 'Item successfully added to your menu.');
                return view('manage_menu/publish-product',compact('sel_product','pro_id'));


            }else{
                if(count($request->preSel)>0) {
                    $pro_id='';
                    $sel_pro=$request->preSel;
                    $sel_product='';
                    $query = DB::table('admin_products')
                        ->join('admin_brand', 'admin_products.brand', '=', 'admin_brand.brand_id')
                        ->join('admin_type', 'admin_products.product_type', '=', 'admin_type.type_id');
                    for($k=0;$k<count($sel_pro);$k++){
                        $pro_id.=$sel_pro[$k].',';
                        $query->orWhere('admin_products.id','=', $sel_pro[$k]);
                    }
                    $sel_product=$query->get();
                    $pro_id=rtrim($pro_id,',');

                    return view('manage_menu/publish-product',compact('sel_product','pro_id'));
                } else{
                    return  redirect()->to('/select-product');
                }
            }


        }else{
            return  redirect()->to('/login');
        }
    }

    public function addRelatedPro(Request $request){

        $proId=($request->input('proId')!='')?$request->input('proId'):'';
        $sql=DB::table('products')->where('prod_id', $proId)->update(array('related_products' => '1'));
        return;
    }

    public function removeRelatedPro(Request $request){
        $proId=($request->input('proId')!='')?$request->input('proId'):'';
        $sql=DB::table('products')->where('prod_id', $proId)->update(array('related_products' => '0'));
        return;
    }

    public function relatedProduct(Request $request){
        checkIsSubscribe();
        $user_id=Auth::User()->id;
        $pre_sel = DB::table('users')->where('id', $user_id)->first();

        $product_type = DB::table('admin_type')->where('parentid', 'NULL')->get();
        $product_brand=DB::table('admin_brand')->orderBy('name', 'asc')->get();

        $related_product = DB::table('products')->where('products.user_id','=', $user_id)->where('products.save_status', '=', '0')->where('products.related_products', '=', '1')->get();


        $product_data='';
        $query='';

        $query = DB::table('products')
            ->where('products.user_id','=', $user_id)->where('products.save_status', '=', '0')->where('products.related_products', '!=', '1')
            ->join('admin_brand', 'products.brand', '=', 'admin_brand.brand_id');

        if(!empty($request->pro_type)){
            $query->where('products.pro_type','=', $request->pro_type);
            $query->orWhere('products.sub_cat','=', $request->pro_type);
        }
        if(!empty($request->brand_type)){
            $query->where('products.brand','=', $request->brand_type);
        }
        if(!empty($request->search_key)){
            $query->Where('products.pro_title', 'Like', "%$request->search_key%");
            $query->orWhere('products.short_desc', 'Like', "%$request->search_key%");
            $query->orWhere('products.pro_desc', 'Like', "%$request->search_key%");
        }
        //Remove pre selected product
        $all_related=0;
        $product_data=$query->paginate(12);
        $url = route("dashboard");
        $html='';
        $k=1;
        foreach ($product_data as $value) {
            $img='';
            if($value->pro_image==''){
                $img=$url.'/default.png';
            }else{
                $img=$url.'/product_images/'.$value->prod_id.'/'.$value->pro_image;
            }

            $html.=' <li>
        <div class="item-box">

        <div class="shop-item" id="shop_item_'.$value->prod_id.'">
        <div class="item-img">
        <div class="border__btm"><img id="pro_image_'.$value->prod_id.'" src="'.$img.'" style="height:100%;" /></div>
        <div class="shop-item-info">
        <div class="shop-item-name"><a href="#" id="pro_title_'.$value->prod_id.'"> '.$value->pro_title.'  </a></div>
        <div class="shop-item-cate"><span>'.$value->name.'</span></div>
        </div>
        </div>
        <div class="item-mask">
        <div class="item-mask-detail">
        <div class="item-mask-detail-ele">
        <a class="select____btn">  <div class="checkbox m-0">
        <label class="p-0">
        <input type="checkbox" class="checkboxes" name="sel_pro[]" id="sel_id_'.$value->prod_id.'" value="'.$value->prod_id.'" onclick="selectSingle('.$value->prod_id.');" >
        <span class="cr"><i class="cr-icon fa fa-check"></i></span>Select</label>
        </div></a>
        </div>
        </div>
        </div>
        </div>
        </div>
        </li>  ';
            $k++;
        }
        if ($request->ajax()) {
            return $html;
        }
        return view('manage_menu/related-product',compact('product_data','product_type','product_brand','all_related','related_product'));
    }

    public function productsListing()
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $sel_product='';
            $pro_id='';
            $user_id=Auth::User()->id;
            /* $query = DB::table('products')
             ->where('products.user_id','=', $user_id)->where('products.save_status', '=', '0')->where('products.related_products', '=', '0')
             ->join('admin_brand', 'products.brand', '=', 'admin_brand.brand_id')
             ->join('admin_type', 'products.pro_type', '=', 'admin_type.type_id');
             $product_data=$query->orderBy('products.prod_id', 'desc');
             $product_data=$query->get();*/

            $query = DB::table('products')
                ->where('products.user_id','=', $user_id)->where('products.save_status', '=', '0');
            $product_data=$query->orderBy('products.prod_id', 'desc');
            $product_data=$query->get();
            return view('manage_menu/product-listing',compact('product_data'));
        } else{
            return  redirect()->to('/login');
        }
    }

    public function save_product()
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $sel_product='';
            $pro_id='';
            $user_id=Auth::User()->id;
            $query = DB::table('products')
                ->where('products.user_id','=', $user_id)->where('products.save_status', '=', '1')
                ->join('admin_brand', 'products.brand', '=', 'admin_brand.brand_id')
                ->join('admin_type', 'products.pro_type', '=', 'admin_type.type_id');
            $product_data=$query->get();

            return view('manage_menu/save_product',compact('product_data'));
        } else{
            return  redirect()->to('auth/login');
        }
    }

    public function productSave(Request $request)
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $totalInvoice = ($request->input('totalInvoice')!='0')?$request->input('totalInvoice'):'1';
            $user_id = Auth::User()->id;
            $member_name = session('member_name');
            $save_status = ($request->input('save_status')!='')?$request->input('save_status'):'0';

            $pro_title = ($request->input('pro_title')!='')?$request->input('pro_title'):'';
            $short_desc =($request->input('short_desc')!='')?$request->input('short_desc'):'';
            $pro_desc =($request->input('pro_desc')!='')?$request->input('pro_desc'):'';
            $pro_price ='0';
            $pro_type = ($request->input('pro_type')!='')?$request->input('pro_type'):'';
            $sub_cat = ($request->input('sub_cat')!='Select')?$request->input('sub_cat'):'';
            $brand = ($request->input('brand')!='')?$request->input('brand'):'';
            $add_date=date("Y-m-d");
            $last_id=DB::table('products')->insertGetId(
                ['user_id' => $user_id,'save_status'=>$save_status,'pro_title'=>addslashes($pro_title),'pro_type'=>addslashes($pro_type),'sub_cat'=>addslashes($sub_cat),'brand'=>addslashes($brand),'short_desc' => addslashes($short_desc),'pro_desc' => addslashes($pro_desc),'pro_price' => $pro_price,
                    'status'=>'1','add_date'=>$add_date,'pro_image'=>'','prod_view'=>'0']  );
            if($last_id!=''){
                $min_price='500000';
                for($i=1;$i<=$totalInvoice;$i++){
                    $question = ($request->input('question_'.$i)!='')?$request->input('question_'.$i):'';
                    $ans = ($request->input('ans_'.$i)!='')?$request->input('ans_'.$i):'';

                    if($question!=''){
                        $insert=DB::table('products_qty')->insertGetId(
                            ['pro_id' => $last_id,'qty' => addslashes($question),'price' =>addslashes($ans)]
                        );

                        echo '<pre>';
                        print_r($insert,1);
                        if($ans<$min_price){
                            $min_price=$ans;
                        }
                    }
                }
                //medical benifiate
                for($k=1;$k<=5;$k++){
                    $name = ($request->input('effects_title_'.$k)!='')?$request->input('effects_title_'.$k):'';
                    $price = ($request->input('effects_per_'.$k)!='')?$request->input('effects_per_'.$k):'';
                    $insert=DB::table('strain_attributes')->insertGetId(['prod_id' => $last_id,'type'=>'1','name' => addslashes($name),'percentage' =>addslashes($price)]);
                }


                $last_id2=DB::table('products')->where('prod_id', $last_id)->update(['pro_price' => $min_price]);

                $images=array();

                if($files=$request->file('images')){
                    if(!is_dir('product_images/'.$last_id.'/')){
                        mkdir('product_images/'.$last_id.'/', 0777, true);
                    }
                    $destinationPath='product_images/'.$last_id.'/';
                    $k=1;
                    foreach($files as $file){
                        $random_name='';
                        $filename='';
                        $random_name=time().rand();
                        $extension=$file->getClientOriginalExtension();
                        $filename=$random_name.'.'.$extension;
                        $file->move($destinationPath,$filename);
                        if($k==1){
                            $sql=DB::table('products')->where('prod_id', $last_id)->update(array('pro_image' => $filename));
                            $insert=DB::table('products_image')->insertGetId(
                                ['pro_id' => $last_id,'image' => addslashes($filename)]  );
                        }else{
                            $insert=DB::table('products_image')->insertGetId(
                                ['pro_id' => $last_id,'image' => addslashes($filename)]  );
                        }
                        $k++;
                    }
                }

            }

            if($save_status=='0'){
                Session::flash('success', 'Product has been added successfully.');
                Session::flash('Product_id', $last_id);
            }else{
                Session::flash('success', 'Product has been save successfully.');
                Session::flash('Product_id', '');
            }
            return  redirect()->to('/menu');

        }else{
            return  redirect()->to('auth/login');
        }
    }

    public function productDetails($id,$pro_id,$title)
    {


        $cur_date=date("Y-m-d");
        $user_data = DB::table('users')->where('username','=', $id)/*->where('exp_date','>=', $cur_date)*/->where('status','=', '1')->first();
        if(count($user_data)>0){
            $user_id = $user_data->id;
            $product_faq=DB::table('products_faq')->where('user_id','=', $user_id)->get();

            $product_data = DB::table('products')->where('products.user_id','=', $user_id)->where('products.save_status', '=', '0')
                ->where('products.prod_id', '=', $pro_id)
                ->join('admin_brand', 'products.brand', '=', 'admin_brand.brand_id')
                ->first();
            if(count($product_data)>0){
                //Update product view
                $pro_view=$product_data->prod_view+1;
                $update_id=DB::table('products')
                    ->where('prod_id', $pro_id)
                    ->update( ['prod_view' => $pro_view]   );

                $product_qty=DB::table('products_qty')->where('pro_id','=', $product_data->prod_id)->get();
                $product_image=DB::table('products_image')->where('pro_id','=', $product_data->prod_id)->get();
                $effects=DB::table('strain_attributes')->where('prod_id','=', $product_data->prod_id)->where('type','=', 1)->where('percentage','!=','')->get();
                $medical=DB::table('strain_attributes')->where('prod_id','=', $product_data->prod_id)->where('type','=', 2)->where('percentage','!=', '')->get();
                $negatives=DB::table('strain_attributes')->where('prod_id','=', $product_data->prod_id)->where('type','=', 3)->where('percentage','!=','')->get();
                $user_name=$user_data->username;
                $bussiness_name=$user_data->company_name;

                $brand='';
                $new='0';
                $total_item=Cart::count();
                if($total_item>0){
                    foreach(Cart::content() as $row){
                        $proid=explode('@@@@@',$row->id);
                        $pre_cart = DB::table('products')
                            ->where('prod_id', '=', $proid[0])->first();
                        if($pre_cart->brand!=''){
                            $brand.=$pre_cart->brand.',';
                        }
                    }
                }

                $related_product='';


                $related_product = DB::table('products')->where('products.user_id','=', $user_id)
                    ->where('products.save_status', '=', '0')->where('products.related_products', '=', '1')
                    ->join('admin_brand', 'products.brand', '=', 'admin_brand.brand_id')
                    ->limit(4)
                    ->get();

                //Get Rating
                $rating_data = DB::table('order_item')->where('prod_id','=', $pro_id)->where('review_status','=', '2')->get();
                $static_data = DB::table('static_contain')->where('user_id','=', $user_id)->first();
                if(count($static_data)>0){
                    $how_it_work=$static_data->how_it_work;
                }else{
                    $how_it_work='';
                }
                // echo $how_it_work;die;
                return view('manage_menu.product-details',compact('product_data','how_it_work','user_name','bussiness_name','product_faq','product_qty','product_image','related_product','effects','negatives','medical','rating_data'));
            }
            else{
                return view('errors.404');
            }
        }else{
            //return  redirect()->to('errors.404');
            return view('errors.404');
        }

    }

    public function updateProduct($id)
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $user_id = Auth::User()->id;
            $cur_date=date("Y-m-d");
            $product_data = DB::table('products')->where('user_id','=', $user_id)->where('prod_id', '=', $id)->first();

            if(count($product_data)>0){
                $brand=DB::table('admin_brand')->orderBy('name', 'asc')->get();
                $sub_cat = DB::table('admin_type')->where('parentid', $product_data->pro_type)->get();
                $type = DB::table('admin_type')->where('parentid', 'NULL')->get();
                $effact = DB::table('strain_attributes')->where('prod_id','=', $id)->where('type','=', '1')->get();
                $item = DB::table('products_qty')->where('pro_id','=', $id)->get();
                $status='none';
                if($product_data->save_status==0){
                    $pro_id=$id;

                }else{
                    $pro_id='0';
                }

                return view('manage_menu/update-product',compact('product_data','brand','type','effact','item','pro_id','sub_cat'));
            }else{
                return  redirect()->to('/products');
            }
        }else{
            return  redirect()->to('/login');
        }
    }

    public function productUpdateSave(Request $request)
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $totalInvoice = ($request->input('totalInvoice')!='0')?$request->input('totalInvoice'):'1';

            $user_id = Auth::User()->id;
            $member_name = Auth::User()->username;
            $save_status = ($request->input('save_status')!='')?$request->input('save_status'):'0';

            $pro_title = ($request->input('pro_title')!='')?$request->input('pro_title'):'';
            $short_desc =($request->input('short_desc')!='')?$request->input('short_desc'):'';
            $pro_desc =($request->input('pro_desc')!='')?$request->input('pro_desc'):'';
            $pro_price ='0';
            $pro_type = ($request->input('pro_type')!='')?$request->input('pro_type'):'';
            $sub_cat = ($request->input('sub_cat')!='Select')?$request->input('sub_cat'):'';
            $brand = ($request->input('brand')!='')?$request->input('brand'):'';
            $add_date=date("Y-m-d");
            $edit_id = $request->input('edit_id');
            $last_id =$edit_id;
            $pro_title=strip_tags($pro_title);
            $pro_desc=strip_tags($pro_desc);
            $short_desc=strip_tags($short_desc);
            $update_id=DB::table('products')->where('prod_id', $edit_id)
                ->update(
                    ['user_id' => $user_id,'save_status'=>$save_status,'pro_title'=>addslashes($pro_title),'pro_type'=>addslashes($pro_type),'sub_cat'=>addslashes($sub_cat),'brand'=>addslashes($brand),'short_desc' => addslashes($short_desc),'pro_desc' => addslashes($pro_desc),'pro_price' => $pro_price,
                        'status'=>'1','prod_view'=>'0']  );
            if($last_id!=''){
                $min_price='500000';
                DB::table('products_qty')->where('pro_id', '=', $edit_id)->delete();
                for($i=1;$i<=$totalInvoice;$i++){
                    $question = ($request->input('question_'.$i)!='')?$request->input('question_'.$i):'';
                    $ans = ($request->input('ans_'.$i)!='')?$request->input('ans_'.$i):'';
                    if($question!=''){
                        $insert=DB::table('products_qty')->insertGetId(
                            ['pro_id' => $last_id,'qty' => addslashes($question),'price' =>addslashes($ans)]
                        );

                        if($ans<$min_price){
                            $min_price=$ans;
                        }
                    }
                }


                //medical benifiate
                DB::table('strain_attributes')->where('prod_id', '=', $edit_id)->delete();
                for($k=1;$k<=5;$k++){
                    $name = ($request->input('effects_title_'.$k)!='')?$request->input('effects_title_'.$k):'';
                    $price = ($request->input('effects_per_'.$k)!='')?$request->input('effects_per_'.$k):'';
                    $insert=DB::table('strain_attributes')->insertGetId(
                        ['prod_id' => $last_id,'type'=>'1','name' => addslashes($name),'percentage' =>addslashes($price)]);
                }
                $last_id2=DB::table('products')->where('prod_id', $last_id)->update(['pro_price' => $min_price]);
                $images=array();

                if($files=$request->file('images')){
                    if(!is_dir('product_images/'.$last_id.'/')){
                        mkdir('product_images/'.$last_id.'/', 0777, true);
                    }
                    $destinationPath='product_images/'.$last_id.'/';
                    $k=1;
                    foreach($files as $file){
                        $random_name='';
                        $filename='';
                        $random_name=time().rand();
                        $extension=$file->getClientOriginalExtension();
                        $filename=$random_name.'.'.$extension;
                        $file->move($destinationPath,$filename);
                        if($k==1){
                            $sql=DB::table('products')->where('prod_id', $last_id)->update(array('pro_image' => $filename));
                            $insert=DB::table('products_image')->insertGetId(
                                ['pro_id' => $last_id,'image' => addslashes($filename)]  );
                        }else{
                            $insert=DB::table('products_image')->insertGetId(
                                ['pro_id' => $last_id,'image' => addslashes($filename)]  );
                        }
                        $k++;
                    }
                }

            }

            if($save_status=='0'){
                Session::flash('success', 'Product has been updated successfully.');
                Session::flash('Product_id', $last_id);

            }else{
                Session::flash('success', 'Product has been save successfully.');
                Session::flash('Product_id', '');
            }
            return  redirect()->to('/update-product/'.$last_id.'');

        }else{
            return  redirect()->to('/login');
        }
    }
    /****************************Delete Product***********************************/

    public function productDelete($edit_id)
    {
        checkIsSubscribe();
        if(Auth::id()!=''){
            DB::table('strain_attributes')->where('prod_id', '=', $edit_id)->delete();
            DB::table('products')->where('prod_id', '=', $edit_id)->delete();
            DB::table('products_qty')->where('pro_id', '=', $edit_id)->delete();
            Session::flash('success', 'Product has been deleted successfully.');

                return  redirect()->back();


        } else{
            return  redirect()->to('/login');
        }
    }


    /**********************Order Section**************************/

    /***************************Admin Pending Orders*********************************/
    public function pending_order($order_id)
    {
        checkIsSubscribe();
        $user_id = Auth::User()->id;
        $user_inf=DB::table('orders')->where('orderid', $order_id)->where('admin_id', $user_id)->first();
        if(Auth::id()!='' && count($user_inf)>0){
            $sql=DB::table('orders')->where('orderid', $order_id)->update(array('status' => '0'));
            Session::flash('success', 'Status has been change successfully.');
            return  redirect()->back();

        } else{
            return  redirect()->to('/login');
        }
    }


    /***************************Admin Cancel Orders*********************************/
    public function cancel_order($order_id)
    {
        checkIsSubscribe();
        $user_id = Auth::User()->id;
        $user_inf=DB::table('orders')->where('orderid', $order_id)->where('admin_id', $user_id)->first();

        if(Auth::id()!='' && count($user_inf)>0){
            $sql=DB::table('orders')->where('orderid', $order_id)->update(array('status' => '1'));
            Session::flash('success', 'Status has been change successfully.');
            return  redirect()->back();

        } else{
            return  redirect()->to('/login');
        }
    }



    /***************************Admin Approve Orders*********************************/
    public function approve_order($order_id)
    {
        checkIsSubscribe();
        $user_id = Auth::User()->id;
        $user_inf=DB::table('orders')->where('orderid', $order_id)->where('admin_id', $user_id)->first();
        if(Auth::id()!='' && count($user_inf)>0){
            $sql=DB::table('orders')->where('orderid', $order_id)->update(array('status' => '2'));
            Session::flash('success', 'Status has been change successfully.');
            return  redirect()->back();

        } else{
            return  redirect()->to('/login');
        }
    }

    /**********************Customer Section**************************/
    public function orderDetails($id,Request $request)
    {
        checkIsSubscribe();
        $user_id = Auth::User()->id;
        $user_inf=DB::table('orders')->where('orderid', $id)->where('admin_id', $user_id)->first();
        if(Auth::id()!='' && count($user_inf)>0){
            $order_deatils = DB::table('orders')->where('admin_id','=', $user_id)->where('orderid','=', $id)->first();
            if(count($order_deatils)>0)
            {
                return view('dashboard.order_details',compact('order_deatils'));
            }else{
                return  redirect()->to('/login');
            }
            // return  redirect()->to('admin/report');
        }else{
            return  redirect()->to('/login');
        }
    }
}
