<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Auth;
use File;
use Illuminate\Support\Facades\Input;
class DashboardController extends Controller
{
    public function index()
    {
        return view('frontpage.index');
    }

    public function Customers(Request $request)
    {
        checkIsSubscribe();

        if(Auth::id()!=''){
            $user_id = Auth::User()->id;

            $query = DB::table('orders')->where('admin_id','=', $user_id);
            if(!empty($request->search_key)){
                $query->Where('fname', 'Like', "%$request->search_key%");
                $query->orWhere('lname', 'Like', "%$request->search_key%");
                $query->orWhere('email', 'Like', "%$request->search_key%");
                $query->orWhere('phone', 'Like', "%$request->search_key%");
            }
            $cus_list = $query->get();
            $data = [];
            $dataSum = [];
            foreach ($cus_list as $row){
                $dataSum[$row->user_id] = isset($dataSum[$row->user_id]) ? $dataSum[$row->user_id] + $row->order_amount:$row->order_amount;
                $data[$row->user_id] = $row;
            }
            return view('dashboard.customers',compact('dataSum','data'));
        }else{
            return  redirect()->to('login');
        }
    }

    public function customerDetails($id,$oid,Request $request)
    {
        checkIsSubscribe();
        if(Auth::id()!=''){
            $user_id=Auth::User()->id;
            $client_id=$id;
            $order_id=$oid;
            $invoice_paid2 = DB::table('orders')
                ->where('admin_id','=', $user_id)->where('order_id','=', $order_id)->first();
            /*debug($user_id);
            debug($client_id);
            debug($order_id);
            debug($invoice_paid2);
            exit;*/
            if(!empty(count($invoice_paid2)))
            {
                return view('dashboard.customer_details',compact('client_id','order_id'));
            }else{
                return  redirect()->to('login');
            }
            // return  redirect()->to('admin/report');
        }
    }

    public function Orders(Request $request)
    {
        checkIsSubscribe();

        if(Auth::id()!=''){
            $user_id=Auth::User()->id;
            $query = DB::table('orders')
                ->where('admin_id','=', $user_id);

            if(!empty($request->search_key)){
                $query->Where('fname', 'Like', "%$request->search_key%");
                $query->orWhere('lname', 'Like', "%$request->search_key%");
                $query->orWhere('order_id', 'Like', "%$request->search_key%");
                $query->orWhere('email', 'Like', "%$request->search_key%");
                $query->orWhere('phone', 'Like', "%$request->search_key%");


            }

            if($request->input('start_date')!='' && $request->input('end_date')!=''){
                list($m,$d,$y) = preg_split('/\//', $request->input('start_date'));
                $start=$y.'-'.$m.'-'.$d;
                list($m,$d,$y) = preg_split('/\//', $request->input('end_date'));
                $end=$y.'-'.$m.'-'.$d;
                $query->whereBetween('order_date', array($start,$end));
            }

            $query->orderBy('order_id', 'desc');
            $order_list=$query->get();
            return view('dashboard.orders',compact('order_list'));
        }else{
            return  redirect()->to('login');
        }
    }

    public function Reviews(Request $request)
    {
        checkIsSubscribe();
        if(Auth::id()!=''){
        $user_id=Auth::User()->id;
            $mail_status='0';
            $pending_review = DB::table('orders')
                ->where('admin_id','=', $user_id)->where('rating_status','=', $mail_status)->get();
            $review_pending=count($pending_review);
            $query = DB::table('orders')
                ->where('admin_id','=', $user_id);
            if(!empty($request->status))
            {
                $query->Where('review_status', '=', $request->status);
            }
            if(!empty($request->start_date) && !empty($request->end_date)){
                list($m,$d,$y) = preg_split('/\//', $request->input('start_date'));
                $start=$y.'-'.$m.'-'.$d;
                list($m,$d,$y) = preg_split('/\//', $request->input('end_date'));
                $end=$y.'-'.$m.'-'.$d;
                $query->whereBetween('order_date', array($start, $end));
            }
            $query->orderBy('orderid', 'desc');
            $review_list=$query->get();

            $mail_contain=DB::table('admin')->where('id','=', '1')->first();
            return view('dashboard.reviews',compact('review_pending','review_list','mail_contain'));

        }else{
            return  redirect()->to('login');
        }
    }

    public function report(Request $request)
    {
        checkIsSubscribe();
        if(Auth::id()!=''){
            $user_id=Auth::User()->id;


            $mail_status='0';
            $invoice_paid2 = DB::table('orders')
                ->where('admin_id','=', $user_id)->where('rating_status','=', $mail_status)->get();
            $review_pendind=count($invoice_paid2);

            //Get current month sale
            $current_date=date('Y-m-d');
            $pre_seven_days=date('Y-m-d', strtotime('-30 days'));
            $month_sales=0;
            $invoice_paid = DB::table('orders')
                ->where('admin_id','=', $user_id)->whereBetween('order_date', array($pre_seven_days, $current_date))->get();

            if(count($invoice_paid)>0)
            {
                foreach($invoice_paid as $key => $paid_data){
                    $month_sales=$month_sales+$paid_data->order_amount;
                }
            }

            $month_customer=0;
            $cus = DB::table('orders')->select('email',DB::raw('sum(order_amount) as order_amount_total','fname'))
                ->where('admin_id','=', $user_id)->whereBetween('order_date', array($pre_seven_days, $current_date))->groupBy('email')->get();
            if(count($cus)>0)
            {
                $month_customer=count($cus);
            }
            //Get most popular product
            $popular_product='';
            $query='';
            $query = DB::table('products')
                ->where('products.user_id','=', $user_id)->where('products.save_status', '=', '0')
                ->join('products_brand', 'products.brand', '=', 'products_brand.brand_id');
            $query->orderBy('products.prod_view', 'desc');
            $query->limit(10);
            $popular_product=$query->get();
            //Get record list
            $k=0;
            $str4='[]';
            $strq='';
            $add_date='[]';
            if($request->input('start_date')!='' && $request->input('end_date')!=''){
                list($m,$d,$y) = preg_split('/\//', $request->input('start_date'));
                $start=$y.'-'.$m.'-'.$d;
                list($m,$d,$y) = preg_split('/\//', $request->input('end_date'));
                $end=$y.'-'.$m.'-'.$d;
            }else{
                $start=date('Y-m-d', strtotime('-3 days'));
                $end=$current_date;
            }
            //for graph
            $dates = array($start);
            $aviableDate='';
            $total_price='0';
            $add_date='[';
            $strq='[';
            while(end($dates) <= $end){
                $add_date.='"'.date('m-d-Y', strtotime($dates[$k])).'"'.',';
                $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));


                $total=0;
                $query1 = DB::table('order_item')
                    ->where('admin_id','=', $user_id)->where('orderdate', "=", $dates[$k]) ;
                if(!empty($request->input('pro_brand'))){
                    $query1->where('pro_brand','=', $request->input('pro_brand'));
                }
                if(!empty($request->input('pro_type'))){
                    $query1->where('pro_category','=', $request->input('pro_type'));
                }
                $invoice_paid5=$query1->get();

                if(count($invoice_paid5)>0)
                {
                    foreach($invoice_paid5 as $key => $paid_data5){
                        $total=$total+number_format((float)$paid_data5->price, 2, '.', '');
                    }
                }else{
                    $total.='0';
                }
                $strq.=$total.',';
                $k++;
            }
            $str4=rtrim($strq,',');

            $str4.=']';

            $add_date=rtrim($add_date,',');
            $add_date.=']';

            $date_sales=0;
            $avg_sales=0;
            $total_cus=0;

            $sel_date=date('M d,Y', strtotime($start)).' To '.date('M d,Y', strtotime($end));
            $query2 = DB::table('order_item')
                ->where('admin_id','=', $user_id)->whereBetween('orderdate', array($start,$end));

            if(!empty($request->input('pro_brand'))){
                $query2->where('pro_brand','=', $request->input('pro_brand'));
            }
            if(!empty($request->input('pro_type'))){
                $query2->where('pro_category','=', $request->input('pro_type'));
            }
            $invoice_paid3=$query2->get();

            if(count($invoice_paid3)>0)
            {
                foreach($invoice_paid3 as $key => $paid_data3){
                    $date_sales=$date_sales+$paid_data3->price;
                }
                $avg_sales=$date_sales/count($invoice_paid3);

            }

            $query5 = DB::table('order_item')->select('user_id')
                ->where('admin_id','=', $user_id)->whereBetween('orderdate', array($start,$end));
            if(!empty($request->input('pro_brand'))){
                $query5->where('pro_brand','=', $request->input('pro_brand'));
            }
            if(!empty($request->input('pro_type'))){
                $query5->where('pro_category','=', $request->input('pro_type'));
            }
            $cus2=$query5->groupBy('user_id')->get();

            if(count($cus2)>0)
            {
                $total_cus=count($cus2);
            }




            $type=DB::table('admin_type')->get();
            $brand=DB::table('admin_brand')->orderBy('name', 'asc')->get();
            //Download CSV
            $download_type=$request->input('download_csv');
            if($download_type==1){
                $query3 = DB::table('order_item')
                    ->where('admin_id','=', $user_id)->whereBetween('orderdate', array($start,$end));
                if(!empty($request->input('pro_brand'))){
                    $query3->where('pro_brand','=', $request->input('pro_brand'));
                }
                if(!empty($request->input('pro_type'))){
                    $query3->where('pro_category','=', $request->input('pro_type'));
                }
                $query3->orderBy('orderdate', 'desc');
                $report_get=$query3->get();

                $tot_record_found=0;

                $tot_record_found=1;
                //First Methos
                $export_data="Order Date,Order Id,Pro Name,Size,Qty,Price,First Name,Last Name,Email Address,Phone Number,Address,Suite/Apartment No,City,State,Zip Code\n";
                if(count($report_get)>0){
                    foreach($report_get as $value){
                        $order_date=date('m-d-Y', strtotime($value->orderdate)) ;
                        $qty=1;
                        $ord_info=DB::table('orders')->where('orderid', $value->order_id)->first();
                        $apartment=($ord_info->apartment!='')?$ord_info->apartment:'NA';
                        $export_data.=$order_date.','.$ord_info->order_id.','.addslashes($value->prod_title).','.$value->prod_size.','.$qty.','.$value->price.','.$ord_info->fname.','.$ord_info->lname.','.$ord_info->email.','.$ord_info->phone.','.addslashes($ord_info->address).','.addslashes($apartment).','.$ord_info->city.','.$ord_info->state.','.$ord_info->zip_code."\n";
                    }
                }else{
                    $export_data.='No record found';
                }
                return response($export_data)
                    ->header('Content-Type','application/csv')
                    ->header('Content-Disposition', 'attachment; filename="report.csv"')
                    ->header('Pragma','no-cache')
                    ->header('Expires','0');
            }
            return view('dashboard.report',compact('type','brand','month_sales','month_customer','popular_product','date_sales','avg_sales','total_cus','sel_date','add_date','str4','review_pendind'));
            // return  redirect()->to('/report');
        }else{
            return  redirect()->to('auth.login');
        }
    }

    public function loadMenu()
    {
        checkIsSubscribe();
        if(Auth::id()!=''){
            $user_id=Auth::User()->id;
            $type=DB::table('admin_type')->where('parentid', 'NULL')->get();
            if(!empty($type)){
                $sub=DB::table('admin_type')->where('parentid', '!=','NULL')->get();
            }

            $brand=DB::table('admin_brand')->orderBy('name', 'asc')->get();
            return view('manage_menu/menu',compact('type','brand','sub'));
        }else{
            return  redirect()->to('/login');
        }
    }

    public function faq()
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $user_id=Auth::User()->id;
            $faq=DB::table('products_faq')->where('user_id', $user_id)->get();
            if(count($faq)==0){
                $question='';
                $ans='';
                $insert=DB::table('products_faq')->insertGetId(
                    ['user_id' => $user_id,'question' => $question,'answer' =>$ans] );
                $faq=DB::table('products_faq')->where('user_id', $user_id)->get();
            }

            return view('manage_menu/faq',compact('faq','static_data'));
        }else{
            return  redirect()->to('/login');
        }
    }

    public function faqSave(Request $request)
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $user_id=Auth::User()->id;
            $user_data3 = DB::table('products_faq')->where('user_id', $user_id)->delete();
            $totalInvoice=$request->input('totalInvoice');
            for($i=1;$i<=$totalInvoice;$i++){
                $question = ($request->input('question_'.$i)!='')?$request->input('question_'.$i):'';

                $ans = ($request->input('ans_'.$i)!='')?$request->input('ans_'.$i):'';
                if($question!=''){
                    $insert=DB::table('products_faq')->insertGetId(
                        ['user_id' => $user_id,'question' => addslashes($question),'answer' =>addslashes($ans)] );
                }
            }


            Session::flash('faq_success', 'FAQ  has been added successfully.');
            Session::flash('success_btn', 'success');
            return  redirect()->to('/faq');
        }else{
            return  redirect()->to('/login');
        }
    }

    public function pageContain()
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $user_id=Auth::User()->id;

            $static_data1 = DB::table('static_contain')->where('user_id', $user_id)->first();
            if($static_data1==''){
                $insert=DB::table('static_contain')->insertGetId(
                    ['user_id' => $user_id,'how_it_work' => '','what_next' =>'','cover_image' =>'','facebook_pixel' =>'','google_analytics' =>''] );
            }
            $static_data = DB::table('static_contain')->where('user_id', $user_id)->first();
            return view('manage_menu/page-contain',compact('static_data'));
        }else{
            return  redirect()->to('/login');
        }
    }

    public function pageconatinsave(Request $request)
    {
        checkIsSubscribe();

        if(Auth::Id()!=''){
            $user_id=Auth::User()->id;
            $user_data = DB::table('static_contain')->where('user_id', $user_id)->first();
            $how_it_work = ($request->input('how_it_work')!='')?$request->input('how_it_work'):'';
            $what_next = ($request->input('what_next')!='')?$request->input('what_next'):'';
            $google_analytics = ($request->input('google_analytics')!='')?$request->input('google_analytics'):'';
            $facebook_pixel = ($request->input('facebook_pixel')!='')?$request->input('facebook_pixel'):'';
            if(count($user_data)>0){
                $last_id=DB::table('static_contain')->where('user_id', $user_id)
                    ->update(['how_it_work' => addslashes($how_it_work),'what_next' =>addslashes($what_next),'facebook_pixel' =>addslashes($facebook_pixel),'google_analytics' =>addslashes($google_analytics)]);

            }else{

                $insert=DB::table('static_contain')->insertGetId(
                    ['user_id' => $user_id,'how_it_work' => addslashes($how_it_work),'what_next' =>addslashes($what_next),'facebook_pixel' =>addslashes($facebook_pixel),'google_analytics' =>addslashes($google_analytics),'cover_image' =>''] );
            }


            if(Input::hasFile('image'))
            {
                $file=Input::file('image');
                $random_name=time();
                $destinationPath='cover_admin_img/';
                $extension=$file->getClientOriginalExtension();
                $filename=$random_name.'.'.$extension;
                $byte=File::size($file); //get size of file
                $uploadSuccess=Input::file('image')->move($destinationPath,$filename);
                $last_id=DB::table('static_contain')->where('user_id', $user_id)
                    ->update(['cover_image' => $filename]);
            }
            Session::flash('faq_success', 'Information  has been updated successfully.');
            Session::flash('success_btn', 'success');
            return  redirect()->back();
        }else{
            return  redirect()->to('/login');
        }
    }
}
