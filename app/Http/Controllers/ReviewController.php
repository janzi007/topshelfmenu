<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ReviewController extends Controller
{

    /**********************Review Section**************************/

    public function review_details($order_id){
        checkIsSubscribe();
        if(Auth::id()!=''){
            $user_id=Auth::User()->id;
            $orderid=base64_decode($order_id);
            $order_info = DB::table('orders')->where('orderid',  $orderid)->where('admin_id','=', $user_id)->first();
            if(count($order_info)>0){
                $order_product = DB::table('order_item')->where('order_id','=', $orderid)->get();
                if(count($order_product)>0){
                    return view('dashboard.review_details',compact('order_product','orderid'));
                }else{
                    return view('errors.404');
                }
            }else{
                return view('errors.404');
            }
        }
        else{
            return view('errors.404');
        }
    }

    public function rating_approve(Request $request){
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $order_id = ($request->input('order_id')!='')?$request->input('order_id'):'';

            $rating_status='2';
            $cur_date=date("Y-m-d");
            if(count($request->input('item_id'))>0){
                foreach ($request->input('item_id') as $key => $value) {
                    $rating='p_'.$value;
                    $comment='comment_'.$value;
                    $rating_user=($request->input($rating)!='')?$request->input($rating):'';
                    $comment_user=($request->input($comment)!='')?$request->input($comment):'';
                    $sql=DB::table('order_item')->where('itemid', $value)->update(['rating_star' => $rating_user,'rating_comment' => addslashes($comment_user)]);
                }

                $sql2=DB::table('orders')->where('orderid', $order_id)->update(['review_status' => '2']);
                $sql2=DB::table('order_item')->where('order_id', $order_id)->update(['review_status' => '2']);
            }
            Session::flash('success', 'Rating has been approve successfully.');
            return  redirect()->to('/reviews');

        }
        else{
            return view('errors.404');
        }
    }

    public function review_mail_update(Request $request){
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $re_message = ($request->input('re_message')!='')?$request->input('re_message'):'';
            $sql=DB::table('admin')->where('id', '1')->update(['re_message' => addslashes($re_message)]);
            return  redirect()->to('/reviews');

        }
        else{
            return view('errors.404');
        }
    }
    /*******************Send Rating Request********************/
    public function ratingRequest(Request $request)
    {
        checkIsSubscribe();
        if(Auth::Id()!=''){
            $user_id=Auth::User()->id;
            $mail_status='0';
            $sel_pro=$request->check;
            if(count($request->check)>0) {
                for($k=0;$k<count($sel_pro);$k++){
                    $value = DB::table('orders')->where('orderid', $sel_pro[$k])->first();
                    $user_inf=DB::table('users')->where('id', $value->admin_id)->first();
                    $cus_inf=DB::table('customer')->where('cus_id', $value->user_id)->first();
                    $username= $user_inf->username;
                    $email=$cus_inf->email;
                    $orderid=base64_encode($value->orderid);
                    $mail_contain=DB::table('admin')->where('id','=', '1')->first();
                    Mail::send('admin.review_mail', ['user_name' => $username, 'cus_info'=> $cus_inf,'order_id'=>$orderid,'mail_contain'=>$mail_contain], function ($message) use ($email)
                    {
                        $message->from('noreply@topshelfmenu.us', 'Review Product');
                        $message->to($email)
                            ->subject('Review Product');
                    });
                    $sql=DB::table('orders')->where('orderid', $value->orderid)->update(array('rating_status' => '1'));
                }
                Session::flash('success', 'Mail has been send successfully.');
                return  redirect()->to('admin/reviews');

            }else{
                Session::flash('success', 'Please select atlest one record for action.');
                return  redirect()->to('/reviews');
            }

        }else{
            return  redirect()->to('/login');
        }
    }
}
