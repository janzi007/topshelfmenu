<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use phpDocumentor\Reflection\Types\Parent_;
use Redirect;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        checkIsSubscribe();
        if(Auth::id()!=''){

            $user_id = session('admin_id');
            $seven_days='0';
            $max_pay='0';
            $max_user='0';
            $popular_product='';
            $myTopUser='';
            $popular_product = '';

            return view('frontpage.home',compact('seven_days','max_pay','max_user','popular_product','myTopUser'));
        }else{
            return  redirect()->to('/login');
        }
    }
    function Logout(){
        if(Auth::check()){
            Auth::logout();    
        }
        return Redirect::route('home');
    }
}
