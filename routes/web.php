<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'DashboardController@index')->name('home');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/select-product',['uses'=>'ProductController@loadMore','as'=>'selectProduct']);
Route::get('/related-product',['uses'=>'ProductController@relatedProduct','as'=>'relatedProduct']);
Route::get('/menu', ['uses'=>'DashboardController@loadMenu', 'as'=>'Menu']);
Route::any('/publish-product', ['uses'=>'ProductController@publish_product','as'=>'PublishProduct']);
Route::any('/products', ['uses'=>'ProductController@productsListing', 'as'=>'EditProduct']);
/*Route::any('/select-product', ['uses'=>'ProductController@select_product','as'=>'SelectProduct']);*/
Route::any('/saved-products', ['uses'=>'ProductController@save_product','as'=>'SavedProduct']);
Route::post('/productSave', ['uses'=>'ProductController@productSave', 'as'=>'productSave']);

Route::get('/{id}/product-details/{pro_id}/{title}', ['uses'=>'ProductController@productDetails', 'as'=>'ProductDetail']);

Route::any('/addRelatedPro', ['uses'=>'ProductController@addRelatedPro', 'as'=>'Addproduct']);
Route::any('/removeRelatedPro', ['uses'=>'ProductController@removeRelatedPro', 'as'=>'RemoveProduct']);


Route::any('/update-product/{id}', 'ProductController@updateProduct');
Route::post('/update_product', ['uses'=>'ProductController@productUpdateSave', 'as'=>'UpdateProduct']);
Route::get('/delete_product/{id}', 'ProductController@productDelete');


Route::any('/pending_order/{id}', 'ProductController@pending_order');
Route::any('/cancel_order/{id}', 'ProductController@cancel_order');
Route::any('/approve_order/{id}', 'ProductController@approve_order');
Route::any('/order_details/{id}/', 'ProductController@orderDetails');

Route::get('/faq', ['uses'=>'DashboardController@faq', 'as'=>'FAQ']);
Route::post('/faqSave', 'DashboardController@faqSave');

Route::get('/page-content', ['uses'=>'DashboardController@pageContain','as'=>'PageContent']);
Route::post('/pageconatinsave', ['uses'=>'DashboardController@pageconatinsave', 'as'=>'SaveContent']);

Route::get('/password', ['uses'=>'AccountController@password', 'as'=>'Password']);
Route::any('/updatepassword',['uses'=>'AccountController@upadtePassword', 'as'=>'UpdatePassword']);
Route::get('/payment', ['uses'=>'AccountController@payment', 'as'=>'Payment']);
Route::post('/payment',['uses'=>'AccountController@paymentPost', 'as'=>'PaymentPost']);
Route::get('/business', ['uses'=>'AccountController@bussiness', 'as'=>'Business']);
Route::post('/updateProfile',['uses'=> 'AccountController@updateProfile', 'as'=>'UpdateProfile']);
Route::any('/report', ['uses'=>'DashboardController@report', 'as'=>'Reports']);
Route::get('/forgot-password', function () {     return view('auth.passwords.email');});
Route::get('/customers', ['uses'=>'DashboardController@Customers', 'as'=>'Customers']);
Route::get('/customer_details/{id}/{oid}',['uses'=>'DashboardController@customerDetails', 'as'=>'customerDetails']);
Route::any('/orders', ['uses'=>'DashboardController@Orders', 'as'=>'Orders']);
Route::any('/reviews', ['uses'=>'DashboardController@Reviews', 'as'=>'Reviews']);
/**********************Manage Review****************************/
Route::any('/ratingRequest', 'ReviewController@ratingRequest');
Route::any('/review_details/{id}', 'ReviewController@review_details');
Route::any('/rating_approve', 'ReviewController@rating_approve');
Route::any('/review_mail_update', 'ReviewController@review_mail_update');
/*******************************Customer SECTION*******************/
Route::get('/customer/login', function () {     return view('customer.login');});
Route::any('/customerr', 'CustomerController@loadIndex');
Route::any('/customer/index', 'CustomerController@loadIndex');
Route::any('/customer/edit-profile', 'CustomerController@edit_profile');
Route::any('/customer/upadtePassword', 'CustomerController@upadtePassword');
Route::post('/customer/updateProfile', 'CustomerController@updateProfile');
Route::any('/customer/logout', 'CustomerController@logout');
Route::post('/customer/login', 'CustomerController@loginUser');
Route::any('/customer/cancel_order/{id}', 'CustomerController@cancel_order');


Route::group(['middleware' => ['auth']], function () {
	Route::get('/logout', 'HomeController@logout')->name('logout');

    /*******************************FRONT SECTION*******************/
    Route::any('/{id}', ['uses'=>'ListProductController@indexView', 'as'=>'Pro']);
    Route::get('/{id}/product-details/{pro_id}/{title}', 'ListProductController@productDetails');
    Route::any('/{id}/checkout', 'ListProductController@checkout');


// Route::get('/{id}', 'ListProductController@indexView');
    Route::get('/{id}/search', [
        'uses' => 'ListProductController@getPrice',
        'as' => 'search'
    ]);
    Route::any('/{id}/addItemCart', 'ListProductController@addItemCart'); //add cart to index
    Route::any('/{id}/addCard', 'ListProductController@addCard'); //add to cart related product
    Route::any('/{id}/product-details/{type}/addCardItem', 'ListProductController@addCardItem');
    Route::any('/{id}/updateCard', 'ListProductController@updateCard');
    Route::any('/{id}/cus_login', 'ListProductController@cus_login');
    Route::any('/{id}/order_confirm', 'ListProductController@order_confirm');
    Route::any('/{id}/order_review/{orderid}', 'ListProductController@order_review');
    Route::any('/{id}/removeProCard', 'ListProductController@removeProCard');
    Route::get('/{id}/review_product/{order_id}', 'ListProductController@order_rating');
    Route::any('/{id}/rating_give', 'ListProductController@rating_give');
    Route::any('/{id}/thank_you', 'ListProductController@thank_you');
    Route::any('/{id}/cus_forgot_password', 'ListProductController@forgot');
    Route::any('/{id}/reset_pass/{forgot}', 'ListProductController@resetpass');
    Route::any('/{id}/resetpassword', 'ListProductController@resetpassword');


    Route::get('/404', function () {
        return abort(404);
    });


    Route::post('/super_admin/productSave', 'AdminProductController@productSave');
    Route::post('/super_admin/productCSVSave', 'AdminProductController@productCSVSave');
    Route::post('/super_admin/relatedProductSave', 'AdminProductController@relatedProductSave');

    /************LOAD Super Admin Index**********************/
//Route::any('/admin', 'DashboardController@loadIndex');
    Route::get('/super_admin', function () {     return view('super_admin.login');});
    Route::get('/super_admin/login', 'AdminDashboardController@index');
    Route::post('/super_admin/admin_login', 'AdminDashboardController@loginUser');
    Route::any('/super_admin/index', 'AdminDashboardController@loadIndex');
    Route::any('/super_admin/edit-profile', 'AdminDashboardController@edit_profile');
    Route::any('/super_admin/upadtePassword', 'AdminDashboardController@upadtePassword');
    Route::any('/super_admin/updateProfile', 'AdminDashboardController@updateProfile');
    /*****************BRAND Setting********************************/
    Route::get('/super_admin/brand', 'AdminDashboardController@brand');
    Route::get('/super_admin/brand/{id}', 'AdminDashboardController@editbrand');
    Route::post('/super_admin/addBrand', 'AdminDashboardController@addBrand');
    Route::get('/super_admin/brand_delete/{id}', 'AdminDashboardController@brandDelete');
    Route::post('/super_admin/brand/updateBrand', 'AdminDashboardController@updateBrand');

    /*****************PRODUCT TYPE Setting********************************/
    Route::get('/super_admin/product_type', 'AdminDashboardController@product_type');
    Route::get('/super_admin/product_type/{id}', 'AdminDashboardController@editType');
    Route::post('/super_admin/addProductType', 'AdminDashboardController@addProductType');
    Route::get('/super_admin/type_delete/{id}', 'AdminDashboardController@typeDelete');
    Route::post('/super_admin/product_type/updateType', 'AdminDashboardController@updateType');
    Route::get('/super_admin/logout', 'AdminDashboardController@logout');

    /*****************Product Setting********************************/
    Route::get('/super_admin/menu', 'AdminDashboardController@loadMenu');
    Route::any('/super_admin/get-subcategory-list', 'AdminDashboardController@subcategoryList');
    Route::get('/super_admin/related_products', 'AdminDashboardController@related_products');



});