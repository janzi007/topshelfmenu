
@extends('layouts.master') 

@section('title', 'Bussiness Settings | Top Shelf menu')


@section('preloader')
	@parent
@stop

@section('menu')
	@parent
@stop

@section('sidebar')
	@parent
@stop


@section('content')

<div class="page-breadcrumb"> 
	<a >Account</a>/<span>Business Settings</span> 
</div>
<div class="row page__main__heading" style="margin-top:20px;">
</div>

<div style="background-color:#fff;">
	<div class="row mt-30">
		<div class="col-md-12">
			<div class="tree-top-content">

				<!-- Tab -->
				<div class="box__shadow" style="background-color:#fff; padding:30px;">
					<div >
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="left__content">
									<div class="tt_green_text">UPDATING YOUR ACCOUNT</div>
									<h1>How it works?</h1>
									<p >Please make sure to enter all the important information for your business in the fields to your right.</p>
									<p>The <strong>Username</strong> you pick will be the name you use to log into your account as well as the name that will create your unique URL. For example, Sean's Canna Shop becomes <a href="#">www.topshelfmenu.us/seanscannashop</a> </p>
									<p>The <strong>Business Name</strong> is the name that will represent your business and menu. </p>
									<p>The <strong>Email Address </strong>is the address Top Shelf Menu uses to email all of your online menu order receipts as well as any communication between us.</p> 
									<p>The <strong>Street Address, City, State and Zip Code </strong>needs to be your physical business address so you customers can find you. </p>
									<p>The <strong>Website</strong> needs top be the exact URL you use for your business. </p>
									<p>To get your business <strong>Logo uploaded</strong> from your computer/tablet/phone simply click on Browse and choose the file. We recommend it to be in 185x50px for best possible resolution. </p>
									<p>Please don't hesitate to contact your representative with any questions that you might have. Or email our Help Desk at <a href="mailto:help@topshelfmenu.us">help@topshelfmenu.us</a></p>
								</div>
							</div>

							<div class="col-md-8 col-sm-6 col-xs-12">
								<div class="form__right__box">
									<h4 class="text-center">Business Information</h4>
									<hr style="width:18%;" />


									{{Form::Open(['url'=>'updateProfile','method'=>'POST','enctype'=>'multipart/form-data', 'role'=>'form','id'=>'form-bussiness'])}}
									{{ csrf_field() }}
														<input type="hidden" name="edit_id" value="{{ csrf_token() }}">
										<div class="row">
											<div class="col-md-11">
												<div class="mb-20">
													<label>Username <span class="text-danger">*</span> </label>
													<input type="text" class="input-group-lg full_input mb-5" name="username" id="username"  pattern="[a-zA-Z0-9-_]{2,64}" required value="{{Auth::User()->username}}" placeholder="User Name" >
													<p> {{URL('/')}}<span id="username" style="color:#22aa00;border: none;"></span></p>
												</div>

												<div class="mb-20">
													<label>Business Name <span class="text-danger">*</span> </label>
													<input type="text" class="input-group-lg full_input mb-5" name="company"   required value="" placeholder="Business Name" >
												</div>

												<div class="mb-20">
													<label>Email Address <span class="text-danger">*</span> </label>
													<input type="email" class="input-group-lg full_input mb-5" name="email"   required value="{{Auth::User()->email}}" placeholder="Email Address" >
												</div>
												<div class="mb-20">
													<div class="row">
														<div class="col-sm-6 col-xs-12">
															<label>Street Address <span class="text-danger">*</span> </label>
															<input type="text" class="input-group-lg full_input mb-5" required name="address"   value="" placeholder="Street Address 1"></div>
														<div class="col-sm-6 col-xs-12">
															<label>Street Address 2 </label>
															<input type="text" class="input-group-lg full_input mb-5"  name="address2"   value="" placeholder="Street Address 2"></div>
													</div>
												</div>

												<div class="mb-20">
													<div class="row">
														<div class="col-sm-6 col-xs-12">
															<label>City <span class="text-danger">*</span></label>
															<input type="text" class="input-group-lg full_input mb-5" required name="city"  value="" placeholder="City"></div>
														<div class="col-sm-6 col-xs-12">
															<div class="mb-25">
																<label>State <span class="text-danger">*</span></label>
																<select name="state" class="form-control cart_quantity">
																	<option value=""> Select State </option>
																		@foreach($state as $name)
																			<option value="{{ $name->name }} "> {{ $name->name }} </option>
																		@endforeach

																</select>
													</div>
												</div>

												<div class="mb-20">
													<div class="row">
														<div class="col-sm-6 col-xs-12">
															<label>Zip Code <span class="text-danger">*</span></label>
															<input type="text" class="input-group-lg full_input mb-5" required name="zip_code"  value="" placeholder="Zip Code"></div>
														<div class="col-sm-6 col-xs-12">
															<label>Website <span class="text-danger">*</span></label>
															<input type="text" class="input-group-lg full_input mb-5" required name="website"  value="" placeholder="Website"></div>
													</div>
												</div>




												<div class="row mb-20">
													<div class="col-sm-7 col-xs-12">
														<div class="mb-10">Upload Your Logo <small>(recommended size 185x500)</small></div>

														<p></p>
														<div class="input-group">
															<input type="file" name="image" id="files-input-upload" style="display:none">
															<input type="text" id="fake-file-input-name" disabled="disabled" placeholder="Choose File" class="input-group-lg browse___input">
															<span class="input-group-btn">
																<button id="fake-file-button-browse" type="button" class="btn browse___btn"> Browse </button>
															</span> 
														</div>

													</div>
												</div>
												<div class="mt-20 text-right" style="margin-bottom: 15px;">
													<button type="submit" name="update_profile" value="update_profile" class="btn___green">Save</button>
												</div>

											</div>
										</div>
									{{Form::Close()}}
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
</div>

@endsection


<style>
	select.cart_quantity{height: 53px !important; max-width:600px; width: 100% !important;}
	.parent___menu {
		padding: 15px 0px 20px 74px !important;
		position: relative;
		top: 0px;
		background: #f4f4f4;
		border-top: 1px solid #ddd;
	}
	.parent___menu ul {
		margin: 0px;
		padding: 0px;
		display: list-item;
	}
	.parent___menu ul li {
		margin: 0px !important;
		padding: 0px !important;
		list-style: circle !important;
		line-height: 30px !important;
	}
	.parent___menu ul li a {
		font-size: 14px !important;
		line-height: 30px;
		height: 30px;
		padding: 0px 10px !important;
		border-left: none !important;
		background-color: transparent !important;
	}
	.parent___menu ul li a:hover {
		border-left: none !important;
		background-color: transparent !important;
	}


</style>
						
@section('footer')
	@parent
@stop


@section('script')
	<script type="text/javascript">
		document.getElementById('fake-file-button-browse').addEventListener('click', function() {
			document.getElementById('files-input-upload').click();
		});

		document.getElementById('files-input-upload').addEventListener('change', function() {
			document.getElementById('fake-file-input-name').value = this.value;

		});
							
		$(function() {
			var $username = $('#username'),
				$output = $('#username_title'),
				keyHandler = function(e) {
					var key = e.which || e.keyCode;

					if (
						// Letters
						key >= 65 && key <= 90 ||
						// Dash and Underscore
						key == 173 ||
						// Numbers
						!e.shiftKey && key >= 48 && key <= 57 ||
						// Numeric keypad
						key >= 96 && key <= 105 ||
						// Backspace and Tab and Enter
						key == 8 || key == 9 || key == 13 ||
						// Home and End
						key == 35 || key == 36 ||
						// Left and Right arrows
						key == 37 || key == 39 ||
						// Del and Ins
						key == 46 || key == 45) {
						updateOutput();

						return true;
					}

					return false;
				},
				updateOutput = function() {
					setTimeout(function() {
						$output.text($username.val());
					}, 50);
				};

			$username.keydown(keyHandler).change(updateOutput);
		});		
	</script>
@endsection





