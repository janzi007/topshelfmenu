
@extends('layouts.master') 

@section('title', 'Password Settings | Top Shelf menu')


@section('preloader')
	@parent
@stop

@section('menu')
	@parent
@stop

@section('sidebar')
	@parent
@stop


@section('content')

<div style="background-color:#fff;">
	<div class="row mt-30">
		<div class="col-md-12">
			<div class="tree-top-content">
				<!-- Tab -->
				<div class="box__shadow" style="background-color:#fff; padding:30px;">
					<style>
						select.cart_quantity{height: 53px !important; max-width:600px; width: 100% !important;}
					</style>

					<div >
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="left__content">
									<div class="tt_green_text">Change password</div>
									<h1>HOW IT WORKS?</h1>
									<p>To change your password enter your old  password first, and then your new one. We advise that you change your password at least once a quarter. Even thought our security system is very high, retail shops generally have employees coming in and out regularly and it’s better to be safe and make sure no old employees has it.</p>
								</div>
							</div>
							<div class="col-md-8 col-sm-6 col-xs-12">
								<div class="form__right__box" style="padding-top:90px; padding-bottom:60px;">
									{{Form::Open(['role'=>'form', 'id'=>'form-register' , 'url'=>'updatepassword', 'method'=>'POST' ])}}{{--
									<form class="" role="form" id="form-register" action="{{route('UpdatePassword')}}" method="post">--}}
									<div class="row">
										@if (Session::has('pass_error'))
											<div class="alert alert-danger" id='errordiv'>{{ Session::get('pass_error') }} <span  onclick="hideErrorDiv()" class="pull-right"  style="color:#933432; font-size: 20px;line-height: 15px; cursor: pointer;" >x</span></div>
										@endif

										@if (Session::has('pass_success'))
											<div class="alert alert-success" id='errordiv'>{{ Session::get('pass_success') }} <span  onclick="hideErrorDiv()" class="pull-right" style="color:#2b542c; font-size: 20px;line-height: 15px;cursor: pointer;" >x</span></div>
										@endif
										<input type="hidden" name="_id" value="{{ csrf_token() }}">
										<input type="hidden" name="edit_id" value="">
										<div class="row">
											<div class="col-md-8 col-md-offset-1">
												<h4 class="text-center">Change Your Password</h4>
												<hr style="width:18%;" />
												<div>
													<div class="row">
														<div class="col-xs-12">
															<input  class="input-group-lg full_input mb-5" type="password" name="old_password" id="old_password" placeholder="Current Password">
															<span id="old_password_error" style="color: red; display: none;"></span></div>
													</div>
												</div>
												<div style="height:1px; margin:20px 0px 25px 0px; background-color:#ddd;"></div>
												<div class="mb-20">
													<div class="row">
														<div class="col-xs-12">
															<input type="password" name="new_password" id="new_password" class="input-group-lg full_input mb-5" placeholder="New Password">
															<span id="new_password_error" style="color: red; display: none;"></span>
														</div>
													</div>
												</div>
												<div class="mb-20">
													<div class="row">
														<div class="col-xs-12">
															<input type="password" name="con_password" id="con_password" class="input-group-lg full_input mb-5" placeholder="Confirm Password">
															<span id="con_password_error" style="color: red; display: none;"></span>
														</div>
													</div>
												</div>
												<div class="mt-20 text-right">
													<button type="submit" name="update_password" value="update_password" class="btn___green" style="width:auto !important; padding:0px 30px;">Update Password</button>
												</div>
											</div>
										</div>
									{{Form::Close()}}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection


<style>
	.parent___menu {
		padding: 15px 0px 20px 74px !important;
		position: relative;
		top: 0px;
		background: #f4f4f4;
		border-top: 1px solid #ddd;
	}
	.parent___menu ul {
		margin: 0px;
		padding: 0px;
		display: list-item;
	}
	.parent___menu ul li {
		margin: 0px !important;
		padding: 0px !important;
		list-style: circle !important;
		line-height: 30px !important;
	}
	.parent___menu ul li a {
		font-size: 14px !important;
		line-height: 30px;
		height: 30px;
		padding: 0px 10px !important;
		border-left: none !important;
		background-color: transparent !important;
	}
	.parent___menu ul li a:hover {
		border-left: none !important;
		background-color: transparent !important;
	}


</style>
						
@section('footer')
	@parent
@stop


@section('script')
	<script type="text/javascript">
		
	</script>
@endsection





