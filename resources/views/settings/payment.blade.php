
@extends('layouts.master') 

@section('title', 'Payment Settings | Top Shelf menu')


@section('preloader')
	@parent
@stop

@section('menu')
	@parent
@stop

@section('sidebar')
	@parent
@stop


@section('content')
<div style="background-color:#fff;">
	<div class="row mt-30">
		<div class="col-md-12">
			<div class="tree-top-content">

				<!-- Tab -->

				<div class="box__shadow" style="background-color:#fff; padding:30px;">
					<style>
						select.cart_quantity{height: 53px !important; max-width:600px; width: 100% !important;}
					</style>


					<div >
						<div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="left__content">
									<div class="tt_green_text">update payment Information</div>
									<h1 style="text-transform:uppercase;">How it works?</h1>
									<p>You can edit or add your billing information here at any time. This is the card we will charge on a monthly basis to renew your subscription to Top Shelf Menu. If you have any questions feel free to email or chat with us.</p>
								</div>
							</div>
							<div class="col-md-8 col-sm-6 col-xs-12">
								<div class="form__right__box" style="padding-top:90px; padding-bottom:60px;">
									<h4 class="text-center">Billing Information</h4>
									<hr style="width:18%;" />
									<form action="{{route('PaymentPost')}}" method="POST" id="payment-form">
										{{ csrf_field() }}
										<input type="hidden" name="edit_id" value="{{ csrf_token() }}">

										<span class="payment_errors" id="payment_errors" style="color: red;"></span>
										<div class="row">
											<div class="col-md-11">
												<div class="mb-20">
													<div class="row">
														<div class="col-sm-6 col-xs-12"><label>Credit Card Number <span class="text-danger">*</span>
															</label><input type="text" class="input-group-lg full_input mb-5" data-stripe="number"  required placeholder="Credit Card Number">
														</div>
														<div class="col-sm-6 col-xs-12"><label>CVV <span class="text-danger">*</span> </label>
															<input type="text" class="input-group-lg full_input mb-5" maxlength="4" size="4" data-stripe="cvc"  required placeholder="CVV">
														</div>
													</div>
												</div>

												<div style="background-color:#f4f4f4 !important; border:1px solid #ddd; padding:20px; margin-bottom:25px;">
													<div class="mb-5"><b>Expiration</b></div>
													<div class="row">
														<div class="col-sm-6 col-xs-12">
															<label>Month <span class="text-danger">*</span> </label>
															<select class="input-group-lg full_input mb-5" data-init-plugin="select2"  data-stripe="exp_month">
																<option>Month <i class="fal fa-chevron-down"></i></option>
																<option value="01">Jan (01)</option>
																<option value="02">Feb (02)</option>
																<option value="03">Mar (03)</option>
																<option value="04">Apr (04)</option>
																<option value="05">May (05)</option>
																<option value="06">June (06)</option>
																<option value="07">July (07)</option>
																<option value="08">Aug (08)</option>
																<option value="09">Sep (09)</option>
																<option value="10">Oct (10)</option>
																<option value="11">Nov (11)</option>
																<option value="12">Dec (12)</option>

															</select></div>
														<div class="col-sm-6 col-xs-12">
															<label>Year <span class="text-danger">*</span> </label>
															<select class="input-group-lg full_input mb-5" data-init-plugin="select2" data-stripe="exp_year">
																<option value=''>Year <i class="fal fa-chevron-down"></i></option>
																<option value="2019">2019</option>
																<option value="2020">2020</option>
																<option value="2021">2021</option>
																<option value="2022">2022</option>
																<option value="2023">2023</option>
																<option value="2024">2024</option>
																<option value="2025">2025</option>
																<option value="2026">2026</option>
																<option value="2027">2027</option>
																<option value="2028">2028</option>
																<option value="2029">2029</option>
																<option value="2030">2030</option>
															</select>
														</div>
													</div>
												</div>


												<div class="mt-20 text-right">
													<button type="submit" class="btn___green">Save</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
>

@endsection


<style>
	.parent___menu {
		padding: 15px 0px 20px 74px !important;
		position: relative;
		top: 0px;
		background: #f4f4f4;
		border-top: 1px solid #ddd;
	}
	.parent___menu ul {
		margin: 0px;
		padding: 0px;
		display: list-item;
	}
	.parent___menu ul li {
		margin: 0px !important;
		padding: 0px !important;
		list-style: circle !important;
		line-height: 30px !important;
	}
	.parent___menu ul li a {
		font-size: 14px !important;
		line-height: 30px;
		height: 30px;
		padding: 0px 10px !important;
		border-left: none !important;
		background-color: transparent !important;
	}
	.parent___menu ul li a:hover {
		border-left: none !important;
		background-color: transparent !important;
	}

	}
</style>
						
@section('footer')
	@parent
@stop


@section('script')
	<script type="text/javascript">
		
	</script>
@endsection





