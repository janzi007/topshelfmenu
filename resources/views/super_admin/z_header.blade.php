<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Trees Pot Shop</title>
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="nileforest">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

<link rel="shortcut icon" type="image/x-icon" href="{{asset('/admin/images/favicon.png')}}">
<link rel="icon" type="image/png" href="{{asset('/admin/images/favicon.png')}}">
<link rel="apple-touch-icon" href="{{asset('/admin/images/favicon.png')}}">

<!-- CSS -->
<link href="{{asset('/admin/css/style.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('/admin/css/bootstrap.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('/admin/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('/admin/css/sidebar-menu.css')}}" rel="stylesheet" type="text/css" />
