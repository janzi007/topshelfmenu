<!-- JS -->

<script src="{{asset('/admin/js/jquery-1.11.2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/js/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="{{asset('/admin/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/js/jquery.fitvids.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/js/jquery.viewportchecker.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/js/sidebar-menu.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/js/theme.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/js/jquery.colorbox-min.js')}}" type="text/javascript"></script>
<script src="{{asset('/admin/js/smooth-products.js')}}"></script>

<script src="{{asset('/admin/js/extra/common.js')}}"></script>
	<link class="main-stylesheet" href="{{asset('/admin/js/extra/common.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('/admin/js/extra/validation.js'.time())}}"></script>
<script type="text/javascript">        
   $(window).load(function () {
            $('.sp-wrap').smoothproducts();
        });    </script>
<script src="{{asset('/admin/js/jquery.stellar.min.js')}}" type="text/javascript"></script>


