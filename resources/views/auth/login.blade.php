@include('admin.z_header')
<link href="{{URL::to('/')}}/admin/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="{{URL::to('/')}}/admin/css/smooth-products.css" rel="stylesheet" />
</head>

<body>
<!-- Site Wraper -->
<div class="wrapper"  style="overflow:hidden !important;">

    <div class="row">
        @include('admin.z_login_cover')

        <div class="col-md-8 col-sm-6 col-xs-12">
            <div class="right__form__outer">
                <div class="text-right">
                    <span class="top_____right">Don't have an account?</span><a href="{{route('register')}}" class="btn__white__border">Register</a>
                </div>

                <div class="form_____outer">
                    <div class="form____title mb-0">Sign in to Trees Pot Shop.</div>
                    <p class="color__80 mb-15">Enter your details below.</p>
                    <form id="form-login"  role="form" method="POST" action="{{ route('login') }}" autocomplete="off">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="status" value="1">
                        <div class="row">
                            <div class="col-md-11">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-mb-20">
                                        <input id="email" type="email" class="input-group-lg full_input" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                                    <div class="col-mb-20">
                                        <input id="password" type="password" class="input-group-lg full_input" name="password" placeholder="Password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <a href="{{ route('password.request') }}" class="theme__color"><b>Forgot Password?</b></a>



                                <div class="mt-20 text-center">
                                    <button type="submit" class="btn___green">Sign In</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>
<!-- Site Wraper End -->
@include('admin.z_footer')


</body>
</html>
{{--
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
--}}
