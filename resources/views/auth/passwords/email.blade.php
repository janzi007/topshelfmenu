@include('admin.z_header')
<link href="{{URL::to('/')}}/admin/css/jquery-ui.css?5" rel="stylesheet" type="text/css" />
<link href="{{URL::to('/')}}/admin/css/smooth-products.css?2" rel="stylesheet" />
</head>

<body>
<!-- Site Wraper -->
<div class="wrapper"  style="overflow:hidden !important;">

    <div class="row">
        @include('admin.z_login_cover')

        <div class="col-md-8 col-sm-6 col-xs-12">
            <div class="right__form__outer">
                <div class="text-right">
                    <span class="top_____right">Already have an account?</span><a href="{{route('login')}}" class="btn__white__border">Sign In</a>
                </div>

                <div class="form_____outer">
                    <div class="form____title mb-0">Forgot password to Trees Pot Shop.</div>
                    <p class="color__80 mb-15">Please enter your email address. You will receive a link to create a new password..</p>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="mb-20">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="mt-20 text-center">
                                    <button type="submit" class="btn___green">Reset Password</button>
                                </div>

                            </div>
                        </form>
                    </div>
                                                           </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>
<!-- Site Wraper End -->
@include('admin.z_footer')
</body>