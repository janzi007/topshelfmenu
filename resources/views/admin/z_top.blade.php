<!-- Preloader -->
<section id="preloader">
	<div class="loader" id="loader">
		<div class="loader-img"></div>
	</div>
</section>
<!-- End Preloader -->

<header id="header" class="header">
	<div class="header-inner" style="padding:0px !important;">
		<div class="row" style="margin:0px; padding:0px;">
			<div class="col-xs-6 col-sm-3 plr-0"> </div>
			<div class="col-xs-6 col-sm-6 plr-0 hidden-xs">

			</div>
			<div class="col-xs-6 col-sm-3 plr-0">
				<div class="right__author" style="margin-top: 13px;">
					<a href="{{URL::to('/')}}/{{Auth::user()->username}}" target="_blank" class="">{{ Auth::User()->first_name }}<br>
						<span>{{ Auth::User()->company_name }} </span>
					</a>
					<p><small>Click to View Menu</small></p>
				</div>
			</div>
		</div>
	</div>
</header>
<nav class="main-menu">
	<div class="tt__left__menu">
		<ul>
			<li class="logo__head">
				<a href="{{route('dashboard')}}" style="border-color:#22aa00 !important; background-color:transparent !important; ">
					<span class="tt__logo">
						<img src="{{asset('/admin/images/top-logo.png')}}" srcset="{{asset('/admin/images/logo@2x.png')}} 2x" alt="">
					</span>
				</a>
				<img src="{{asset('/admin/images/head_blur_bg.png')}}" class="hidden-xs" style="position:fixed; top:0px; right:0px;" />
				<img src="{{asset('/admin/images/head_blur_bg_mini.png')}}" class="visible-xs" style="position:fixed; top:0px; right:0px;" />

			</li>
			<li>
				<a href="{{route('dashboard')}}" class="menu_top_margin {{ Request::is('frontpage/home') ? 'active' : '' }}">
					<span class="nav-icon"><img src="{{asset('/admin/images/icon/home.png')}}" /></span> Home</a>
			</li>

			<li>
				<a data-toggle="collapse" data-target="#demo" class="
																	 {{ Request::is('manage_menu/menu') ? 'active' : '' }}
																	 {{ Request::is('manage_menu/saved-products') ? 'active' : '' }}
																	 {{ Request::is('manage_menu/selectProduct') ? 'active' : '' }}
																	 {{ Request::is('admin/product_type') ? 'active' : '' }}
																	 {{ Request::is('admin/brand') ? 'active' : '' }}
																	 {{ Request::is('manage_menu/publish-product') ? 'active' : '' }}
																	 {{ Request::is('manage_menu/products') ? 'active' : '' }}
																	 {{ Request::is('manage_menu/related-product') ? 'active' : '' }}
																	 {{ Request::is('admin/publish-related-product') ? 'active' : '' }}
																	 {{ Request::is('manage_menu/page-contain') ? 'active' : '' }}

																	 {{ Request::is('manage_menu/faq') ? 'active' : '' }}"><span class="nav-icon"><img src="{{asset('/admin/images/icon/menu.png')}}" /></span> Manage Menu <i class="fal fa-chevron-down pull-right" style="margin-top: 8px;"></i></a>

				<div id="demo" class="collapse">
					<div class="parent___menu">
						<ul>
							<li><a href="{{route('selectProduct')}}"> Add Pre-Loaded Products</a></li>
							<li><a href="{{route('relatedProduct')}}"> Set Related Products</a></li>
							<li><a href="{{route('Menu')}}"> Add Products Manually</a></li>
							<li><a href="{{route('EditProduct')}}"> Edit Your Products</a></li>
							<li><a href="{{route('SavedProduct')}}"> Saved Products</a></li>
							<li><a href="{{route('FAQ')}}"> Product Page FAQs</a></li>
							<li><a href="{{route('PageContent')}}"> Page Content</a></li>

						</ul>
					</div>
				</div>
			</li>


			<li>
				<a href="{{route('Customers')}}" class="{{ Request::is('dashboard/customers') ? 'active' : '' }} ">
					<span class="nav-icon"><i class="far fa-users fa-lg"></i></span> Customers</a>
			</li>


			<li>
				<a href="{{route('Orders')}}" class="{{ Request::is('dashboard/orders') ? 'active' : '' }} ">
					<span class="nav-icon"><img src="{{asset('/admin/images/icon/customers.png')}}" /></span> Orders</a>
			</li>

			<li>
				<a href="{{route('Reviews')}}" class="{{ Request::is('dashboard/reviews') ? 'active' : '' }} ">
					<span class="nav-icon"><i class="far fa-comments fa-lg"></i></span> Reviews </a>
			</li>

			<li>
				<a href="{{route('Reports')}}" class="{{ Request::is('dashboard/report') ? 'active' : '' }} ">
					<span class="nav-icon"><img src="{{asset('/admin/images/icon/report.png')}}" /></span> Reports</a>
			</li>


			<li>
				<a data-toggle="collapse" data-target="#demo1" class=""><span class="nav-icon"><img src="{{asset('/admin/images/icon/setting.png')}}" /></span> Settings  <i class="fal fa-chevron-down pull-right" style="margin-top: 8px;"></i></a>
				<div id="demo1" class="collapse">
					<div class="parent___menu">
						<ul>
							<li><a href="{{route('Business')}}" class="{{Request::is('settings/bussiness')?'active':''}}"> Business Settings</a></li>
							<li><a href="{{route('Payment')}}" class="{{Request::is('settings/payment')?'active':''}}"> Payment Information</a></li>
							<li><a href="{{route('Password')}}" class="{{Request::is('settings/password')?'active':''}}"> Password</a></li>

						</ul>
					</div>
				</div>
			</li>
			<li>
				<a href="{{URL::to('/')}}/{{Auth::user()->username}}" target="_blank" class="">
					<span class="nav-icon"><i class="fal fa-list-alt fa-lg"></i></span> View Menu</a>
			</li>
			<li><hr></li>
			<li>
				<a href="javascript:void(0);" onclick="olark('api.box.expand')" class="{{ Request::is('admin/help') ? 'active' : '' }}">
					<span class="nav-icon"><img src="{{asset('/admin/images/icon/help.png')}}" /></span> Help</a>
			</li>
			<li>
				<a href="{{route('logout')}}"><span class="nav-icon"><img src="{{asset('/admin/images/icon/logout.png')}}" /></span> Logout</a>
			</li>
		</ul>
	</div>
</nav>

<style>
	.alert__position {
	margin-left: 80px;
	position: relative;
	border-radius: 0px !important;
	padding: 8px 28px !important;
	top: 80px;
}
@media (max-width: 767px) {
	.alert__position {
		margin-left: 60px;
		padding: 8px 28px !important;
		top: 65px;
	}
}
.parent___menu {
	padding: 15px 0px 20px 74px !important;
	position: relative;
	top: 0px;
	background: #f4f4f4;
	border-top: 1px solid #ddd;
}
.parent___menu ul {
	margin: 0px;
	padding: 0px;
	display: list-item;
}
.parent___menu ul li {
	margin: 0px !important;
	padding: 0px !important;
	list-style: circle !important;
	line-height: 30px !important;
}
.parent___menu ul li a {
	font-size: 14px !important;
	line-height: 30px;
	height: 30px;
	padding: 0px 10px !important;
	border-left: none !important;
	background-color: transparent !important;
}
.parent___menu ul li a:hover {
	border-left: none !important;
	background-color: transparent !important;
}
</style>