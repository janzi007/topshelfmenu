<header id="header" class="header">
	<div class="header-inner" style="padding:0px !important;">
		<div class="row" style="margin:0px; padding:0px;">
			<div class="col-xs-6 col-sm-3 plr-0"> </div>
			<div class="col-xs-6 col-sm-6 plr-0 hidden-xs">
				<div class="logo">
					@if (!empty(Session::has('member_logo')))
						<img src="{{URL::asset('/')}}/profile/{{ Session::get('member_logo') }}"  class="" style="max-height:50px; margin-top:13px;" >
					@else
						<img src="{{URL::asset('/')}}/admin/images/pro-avatar.png" class="" style="max-height:50px; margin-top:13px;" >
					@endif 
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 plr-0">
				<div class="right__author" style="margin-top: 13px;">
					<a href="{{URL::to('/')}}/{{ Session::get('member_name') }}" target="_blank" class="">
						<span>{{ Session::get('member_company') }} </span>
					</a>
					<p><small>Click to View Menu</small></p>
				</div>
			</div>
		</div>
	</div>
</header>	