	<nav class="main-menu">
		<div class="tt__left__menu">
			<ul>
				<li class="logo__head">
					<a href="{{URL::to('/')}}/admin/index" style="border-color:#22aa00 !important; background-color:transparent !important; ">
						<span class="tt__logo">
							<img src="{{URL::to('/')}}/admin/images/top-logo.png" srcset="{{URL::to('/')}}/admin/images/logo@2x.png 2x" alt="">
						</span>
					</a>
					<img src="{{URL::to('/')}}/admin/images/head_blur_bg.png" class="hidden-xs" style="position:fixed; top:0px; right:0px;" />
					<img src="{{URL::to('/')}}/admin/images/head_blur_bg_mini.png" class="visible-xs" style="position:fixed; top:0px; right:0px;" />

				</li>
				<li>
					<a href="{{URL::to('/')}}/admin/index" class="menu_top_margin {{ Request::is('admin/index') ? 'active' : '' }}">
						<span class="nav-icon"><img src="{{URL::to('/')}}/admin/images/icon/home.png" /></span> Home</a>
				</li>
				<li>
					<a data-toggle="collapse" data-target="#demo" class="
																		 {{ Request::is('admin/menu') ? 'active' : '' }}
																		 {{ Request::is('admin/saved-products') ? 'active' : '' }}
																		 {{ Request::is('admin/select-product') ? 'active' : '' }}
																		 {{ Request::is('admin/product_type') ? 'active' : '' }}
																		 {{ Request::is('admin/brand') ? 'active' : '' }}
																		 {{ Request::is('admin/publish-product') ? 'active' : '' }}
																		 {{ Request::is('admin/products') ? 'active' : '' }}
																		 {{ Request::is('admin/related-product') ? 'active' : '' }}
																		 {{ Request::is('admin/publish-related-product') ? 'active' : '' }}
																		 {{ Request::is('admin/page-contain') ? 'active' : '' }}

																		 {{ Request::is('admin/faq') ? 'active' : '' }}"><span class="nav-icon"><img src="{{URL::to('/')}}/admin/images/icon/menu.png" /></span> Manage Menu <i class="fal fa-chevron-down pull-right" style="margin-top: 8px;"></i></a>

					<div id="demo" class="collapse">
						<div class="parent___menu">
							<ul>
								<li><a href="{{URL::to('/')}}/admin/select-product"> Add Pre-Loaded Products</a></li>
								<li><a href="{{URL::to('/')}}/admin/related-product"> Set Related Products</a></li>
								<li><a href="{{URL::to('/')}}/admin/menu"> Add Products Manually</a></li>
								<li><a href="{{URL::to('/')}}/admin/products"> Edit Your Products</a></li>
								<li><a href="{{URL::to('/')}}/admin/saved-products"> Saved Products</a></li>
								<li><a href="{{URL::to('/')}}/admin/faq"> Product Page FAQs</a></li>
								<li><a href="{{URL::to('/')}}/admin/page-contain"> Page Content</a></li>

							</ul>
						</div>
					</div>
				</li>
				<li>
					<a href="{{URL::to('/')}}/admin/customers" class="{{ Request::is('admin/customers') ? 'active' : '' }} ">
						<span class="nav-icon"><i class="far fa-users fa-lg"></i></span> Customers</a>
				</li>
				<li>
					<a href="{{URL::to('/')}}/admin/orders" class="{{ Request::is('admin/orders') ? 'active' : '' }} ">
						<span class="nav-icon"><img src="{{URL::to('/')}}/admin/images/icon/customers.png" /></span> Orders</a>
				</li>
				<li>
					<a href="{{URL::to('/')}}/admin/reviews" class="{{ Request::is('admin/reviews') ? 'active' : '' }} ">
						<span class="nav-icon"><i class="far fa-comments fa-lg"></i></span> Reviews </a>
				</li>
				<li>
					<a href="{{URL::to('/')}}/admin/report" class="{{ Request::is('admin/report') ? 'active' : '' }} ">
						<span class="nav-icon"><img src="{{URL::to('/')}}/admin/images/icon/report.png" /></span> Reports</a>
				</li>
				<li>
					<a data-toggle="collapse" data-target="#demo1" class="{{Request::is('admin/bussiness')?'active':''}} {{ Request::is('admin/payment')?'active':''}} {{ Request::is('admin/password')?'active':'' }}">
						<span class="nav-icon">
							<img src="{{URL::to('/')}}/admin/images/icon/setting.png" />
						</span> Settings  <i class="fal fa-chevron-down pull-right" style="margin-top: 8px;"></i>
					</a>
					<div id="demo1" class="collapse">
						<div class="parent___menu">
							<ul>
								<li><a href="{{URL::to('/')}}/admin/bussiness" class="{{Request::is('admin/bussiness')?'active':''}}"> Business Settings</a></li>
								<li><a href="{{URL::to('/')}}/admin/payment" class="{{Request::is('admin/payment')?'active':''}}"> Payment Information</a></li>
								<li><a href="{{URL::to('/')}}/admin/password" class="{{Request::is('admin/password')?'active':''}}"> Password</a></li>

							</ul>
						</div>
					</div>
				</li>
				<li>
					<a href="{{URL::to('/')}}/{{ Session::get('member_name') }}" target="_blank" class="">
						<span class="nav-icon">
							<i class="fal fa-list-alt fa-lg"></i>
						</span> View Menu
					</a>
				</li>
				<li><hr></li>
				<li>
					<a href="javascript:void(0);" onclick="olark('api.box.expand')" class="{{ Request::is('admin/help') ? 'active' : '' }}">
						<span class="nav-icon">
							<img src="{{URL::to('/')}}/admin/images/icon/help.png" /></span> Help</a>
				</li>
				<li>
					<a href="{{URL::to('/')}}/admin/logout"><span class="nav-icon"><img src="{{URL::to('/')}}/admin/images/icon/logout.png" /></span> Logout</a>
				</li>
			</ul>
		</div>
	</nav>	