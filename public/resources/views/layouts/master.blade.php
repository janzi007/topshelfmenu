<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
        <title>@yield('title')</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<link rel="apple-touch-icon" sizes="180x180" href="{{URL::asset('/')}}/images/apple-touch-icon.png">
		<link rel="shortcut icon" href="{{URL::asset('/')}}favicon.ico">
		<link rel="icon" type="image/png" sizes="32x32" href="{{URL::asset('/')}}favicon.ico">
		<link rel="icon" type="image/png" sizes="32x32" href="{{URL::asset('/')}}favicon.ico">
		<meta name="msapplication-config" content="{URL::asset('/')}}/browserconfig.xml">
		<meta name="theme-color" content="#ffffff">

		<!-- CSS -->
		<link href="{{URL::asset('/')}}/admin/css/style.css" rel="stylesheet" type="text/css" />
		<link href="{{URL::asset('/')}}/admin/css/bootstrap.css" rel="stylesheet" type="text/css" />
		<link href="{{URL::asset('/')}}/admin/css/font-awesome.css" rel="stylesheet" type="text/css" />
		<link href="{{URL::asset('/')}}/admin/css/sidebar-menu.css" rel="stylesheet" type="text/css" />
		<link href="{{URL::asset('/')}}/admin/js/extra/common.css" rel="stylesheet" type="text/css" class="main-stylesheet" />
		<script src="{{URL::asset('/')}}/admin/js/fontawesome-all.js"></script>
		<script src="{{URL::asset('/')}}/admin/js/fa-v4-shims.min.js"></script>
    </head>

	<!-- Start Nav Menu -->
	@section('menu')
		@include('layouts.menu')
	@show
	<!-- End Nav Menu -->

	<!-- Start Sidebar -->
	@section('sidebar')
		@include('layouts.sidebar')
	@show
	<!-- End Sidebar -->
		
    <body>
        <div class="wrapper">
			<div class="clearfix"></div>
			<section class="main__container">        
            	@yield('content')
            </section>
        </div>
        
        @section('footer')
        <script src="{{URL::asset('/')}}/admin/js/jquery-1.11.2.min.js" type="text/javascript"></script>
		<script src="{{URL::asset('/')}}/admin/js/jquery-ui.min.js" type="text/javascript"></script>
		<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
		<script src="{{URL::asset('/')}}/admin/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="{{URL::asset('/')}}/admin/js/jquery.fitvids.js" type="text/javascript"></script>
		<script src="{{URL::asset('/')}}/admin/js/jquery.viewportchecker.js" type="text/javascript"></script>
		<script src="{{URL::asset('/')}}/admin/js/sidebar-menu.js" type="text/javascript"></script>
		<script src="{{URL::asset('/')}}/admin/js/theme.js" type="text/javascript"></script>
		<script src="{{URL::asset('/')}}/admin/js/jquery.colorbox-min.js" type="text/javascript"></script>
		<script src="{{URL::asset('/')}}/admin/js/smooth-products.js"></script>
		<script src="{{URL::asset('/')}}/admin/js/extra/common.js"></script>

		<script src="{{URL::asset('/')}}/admin/js/extra/validation.js"></script> 
		<script type="text/javascript">        
			$(window).load(function () {
				$('.sp-wrap').smoothproducts();
			});    
		</script>
		<script src="{{URL::asset('/')}}/admin/js/jquery.stellar.min.js" type="text/javascript"></script>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114997354-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-114997354-1');
		</script>


		<!-- begin olark code -->
		<script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");
		/* custom configuration goes here (www.olark.com/documentation) */
		olark.identify('1978-848-10-4767');</script>
		<!-- end olark code -->
        @show
        
        @yield('script')
    </body>
</html>